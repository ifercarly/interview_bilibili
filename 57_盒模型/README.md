## 是什么

盒模型就是组成一个块级盒子的各个部分，包括 Content、Padding、Border、Margin。

## 分类

盒模型分为两种，分别是标准盒模型和怪异（替代/IE/CSS3）盒模型。

## 切换

可以通过 box-sizing 进行切换，默认 `box-sizing: content-box` 表示标准盒模型，设置 `box-sizing: border-box` 就切换为了怪异盒模型。

## 特点/计算

标准盒模型：width = border + padding + 内容的宽度，高度同理。

怪异盒模型：width = 内容的宽度，高度同理。

