# 数组塌陷问题

## 是什么

删除数组中的元素时，会导致被删除元素后面的元素整体往前面移动，造成数组长度减小的现象。

## 影响

循环删除元素时可能会遗漏掉一些元素。

```js
const arr = [1, 2, 3, 4, 5, 6]
for(let i = 0; i < arr.length; i ++) {
  arr.splice(i, 1)
}
console.log(arr)
```

## 解决

1\. 每次删完让 i 减一。

2\. 倒着删。

## 应用

数组去重。

```js
const arr = [1, 2, 3, 1, 1, 6]
for (let i = 0; i < arr.length; i++) {
  for (let j = i + 1; j < arr.length; j++) {
    if (arr[i] === arr[j]) {
      arr.splice(j, 1)
      // 解决 splice 后带来的数组塌陷问题
      // [1, 2, 3, 1, 1, 6]
      // 例如把索引为 3 的 1 删了，数据变成了 [1, 2, 3, 1, 6]
      // 下一次 j++ 变成了 4
      // 会发现直接和 6 进行了比较，少比较了一个 1，所以需要 j--
      // j--
    }
  }
}
console.log(arr)
```
