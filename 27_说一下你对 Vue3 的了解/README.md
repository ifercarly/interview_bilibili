# 说一下你对 Vue3 的理解

## 性能更高了

a, 响应式原理换成了 Proxy。

```js
{
    a: 1,
    b: 2,
    c: 3
}
```



b，VNode Diff 算法进行了优化，PatchFlag [静态标记](https://vue-next-template-explorer.netlify.app/#eyJzcmMiOiI8ZGl2PlxyXG4gIDxwPmhlbGxvPC9wPlxyXG4gIDxwPndvcmxkPC9wPlxyXG4gIDxwPnt7Zm9vfX08L3A+XHJcbjwvZGl2PlxyXG4iLCJvcHRpb25zIjp7fX0=)。

与上次虚拟 DOM 进行比对的时候，只比对带有 patch flag 的节点，并且可以根据 flag 的信息得知要具体比较的内容。

c，hoistStatic 静态提升。

Vue2 中无论元素是否参与更新，每次都会重新创建、渲染；Vue3 中对于不参与更新的元素做了静态提升，只会被创建一次，渲染时直接复用即可。

d，cacheHandlers 事件侦听器缓存。

默认情况下 onClick 会被视为动态绑定，所以每次都会去追踪它的变化；但是因为是同一个函数，所以没有必要追踪变化，直接缓存复用即可。

```html
<div>
  <p @click="handleClick">hello</p>
</div>
```

## 体积更小了

1\. 删除了一些没必要或不常用到的 API，例如 filter、EventBus 等。

2\. 按需导入，能配合 Webpack 支持 Tree Shaking。

3\. ...

## 对 TS 支持更好啦

源码是 TS 重写的。

## Composition API

对于大型项目更利于代码的组织和复用。

## 新特性

Fragment、Teleport、Suspense、...。