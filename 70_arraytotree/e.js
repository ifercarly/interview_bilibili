const list = [
  { id: 'a', pid: '', name: '总裁办' },
  { id: 'b', pid: '', name: '行政部' },
  {
    id: 'c',
    pid: '',
    name: '财务部',
    children: [
      { id: 'd', pid: 'c', name: '财务核算部' },
      {
        id: 'e',
        pid: 'c',
        name: '税务管理部',
        children: [
          { id: 'f', pid: 'e', name: '税务管理 A 部' },
          { id: 'g', pid: 'e', name: '税务管理 B 部' },
        ],
      },
    ],
  },
]

const getTreeToArrayResult = (list) => {
  const arr = []
  const treeToArray = (list) => {
    if (list && list.length > 0) {
      list.forEach((item) => {
        arr.push(item)
        treeToArray(item.children)
        delete item.children
      })
    }
  }
  treeToArray(list)
  return arr
}
const result = getTreeToArrayResult(list)
console.log(result)

