const list = [
  { id: 'a', pid: '', name: '总裁办' },
  { id: 'b', pid: '', name: '行政部' },
  {
    id: 'c',
    pid: '',
    name: '财务部',
    children: [
      { id: 'd', pid: 'c', name: '财务核算部' },
      {
        id: 'e',
        pid: 'c',
        name: '税务管理部',
        children: [
          { id: 'f', pid: 'e', name: '税务管理 A 部' },
          { id: 'g', pid: 'e', name: '税务管理 B 部' },
        ],
      },
    ],
  },
]
const tree2array = (tree) => {
  return tree.reduce((prev, cur) => {
    if(!cur.children) {
      prev.push(cur)
    } else {
      const subList = tree2array(cur.children)
      // 删除 cur 的 children
      delete cur.children
      prev.push(cur, ...subList)
    }
    return prev
  }, [])
}

console.log(tree2array(list))


