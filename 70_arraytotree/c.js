// 如果对象 B 的 pid 等于对象 A 的 id，那么就把整个对象 B 作为对象 A 的 children 属性值
const list = [
  {id: 'a',pid: '',name: '总裁办'},
  {id: 'b',pid: '',name: '行政部'},
  {id: 'c',pid: '',name: '财务部'},
  {id: 'd',pid: 'c',name: '财务核算部'},
  {id: 'e',pid: 'c',name: '税务管理部'},
  {id: 'f',pid: 'e',name: '税务管理 A 部'},
  {id: 'g',pid: 'e',name: '税务管理 B 部'},
]

const ROOT_ID = ''

function fn1(list, id=ROOT_ID) {
  const arr = []
  list.forEach((item) => {
    if (item.pid === id) {
      // 从 list 中继续找，谁的 pid 等于 item 的 id 的
      const children = fn2(list, item.id)
      if(children.length) {
        item.children = children
      }
      arr.push(item)
    }
  })
  return arr
}

function fn2(list, id) {
  const arr = []
  list.forEach((item) => {
    if (item.pid === id) {
      // 从 list 中继续找，谁的 pid 等于 item 的 id 的
      const children = fn3(list, item.id)
      if(children.length) {
        item.children = children
      }
      arr.push(item)
    }
  })
  return arr
}

function fn3(list, id) {
  const arr = []
  list.forEach(item => {
    if(item.pid === id) {
      arr.push(item)
    }
  })
  return arr
}

const result = fn1(list)
console.log(result)