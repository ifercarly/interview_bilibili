const list = [
  { id: 'a', pid: '', name: '总裁办' },
  { id: 'b', pid: '', name: '行政部' },
  {
    id: 'c',
    pid: '',
    name: '财务部',
    children: [
      { id: 'd', pid: 'c', name: '财务核算部' },
      {
        id: 'e',
        pid: 'c',
        name: '税务管理部',
        children: [
          { id: 'f', pid: 'e', name: '税务管理 A 部' },
          { id: 'g', pid: 'e', name: '税务管理 B 部' },
        ],
      },
    ],
  },
  {
    id: 'h',
    pid: '',
    name: '教研部',
    children: [
      { id: 'i', pid: 'h', name: '前端' },
      { id: 'j', pid: 'h', name: 'JAVA' },
    ],
  },
]

function getID(json, id, callback) {
  json.some(function (item) {
    if (item.id == id) {
      callback(item)
      return true
    } else if (item.children && item.children.length > 0) {
      getID(item.children, id, callback)
    }
  })
}
getID(list, 'h', function (data) {
  console.log(data)
})
