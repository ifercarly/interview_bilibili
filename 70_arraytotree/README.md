## 0. 问题

如何把一个有规律的 arr 转成 tree。

## 1. 需求

🙂 如果【对象 D 的 pid】 等于【对象 C 的 id】，那么就把整个对象 D 放到对象 C 的 children 属性数组中，演示如下。

```js
// 原始数组
const list = [
    { id: 'a', pid: '', name: '总裁办' },
    { id: 'b', pid: '', name: '行政部' },
    { id: 'c', pid: '', name: '财务部' }, // 对象 C
    { id: 'd', pid: 'c', name: '财务核算部' }, // 对象 D
    { id: 'e', pid: 'c', name: '税务管理部' },
    { id: 'f', pid: 'e', name: '税务管理 A 部' },
    { id: 'g', pid: 'e', name: '税务管理 B 部' },
]
```

```js
// 期望结果
const list = [
    { id: 'a', pid: '', name: '总裁办' },
    { id: 'b', pid: '', name: '行政部' },
    {
        id: 'c', // 对象 C
        pid: '',
        name: '财务部',
        children: [
            { id: 'd', pid: 'c', name: '财务核算部' }, // 对象 D
            {
                id: 'e',
                pid: 'c',
                name: '税务管理部',
                children: [
                    { id: 'f', pid: 'e', name: '税务管理 A 部' },
                    { id: 'g', pid: 'e', name: '税务管理 B 部' },
                ],
            },
        ],
    },
]
```

## 2. 确定 ROOT_ID

🍜 先确定 ROOT_ID 为 `''`（因为我们总要先找到某些 pid 放到第一层），即找 pid 等于 ROOT_ID 的添加到新数组的第一层，期望结果如下。

```js
const list = [
    { id: 'a', pid: '', name: '总裁办' },
    { id: 'b', pid: '', name: '行政部' },
    { id: 'c', pid: '', name: '财务部' }, // C 对象
]
```

🤔 思路：封装方法 fn1，从 list 中查找，【谁】（数组的某一项 item）的 pid 等于<font color=e32d40>**根 id**</font> 的，就把这个【谁】添加到数组中。

```js
const ROOT_ID = ''

function fn1(list, id = ROOT_ID) {
    const arr = []
    // 从 list 中继续找，谁的 pid 等于根 id 的
    list.forEach((item) => {
        if (item.pid === id) {
            arr.push(item)
        }
    })
    return arr
}
const result = fn1(list)
console.log(result)
```

## 3. 找 item 的 children

添加进去的 item 项可能还有 children，所以添加之前先找有没有 children，怎么找有没有 children 呢？

思路：封装方法 fn2，从 list 中查找，【谁】的 pid 等于 <font color=e32d40>**item 的 id**</font> 的，就把这个【谁】添加到数组中，最后把整个数组作为添加进去的 item 的 children。

```js
const ROOT_ID = ''

function fn1(list, id = ROOT_ID) {
    const arr = []
    list.forEach((item) => {
        if (item.pid === id) {
            const children = fn2(list, item.id)
            if (children.length) {
                item.children = children
            }
            arr.push(item)
        }
    })
    return arr
}

function fn2(list, id) {
    const arr = []
    list.forEach((item) => {
        if (item.pid === id) {
            arr.push(item)
        }
    })
    return arr
}

const result = fn1(list, '')
console.log(result)
```

期望结果如下。

```js
// 期望结果
const list = [
    { id: 'a', pid: '', name: '总裁办' },
    { id: 'b', pid: '', name: '行政部' },
    {
        id: 'c',
        pid: '',
        name: '财务部',
        children: [
            { id: 'd', pid: 'c', name: '财务核算部' },
            { id: 'e', pid: 'c', name: '税务管理部' },
        ],
    },
]
```

## 4. 找 item 的 children

添加进去的 item 项可能还有 children，所以添加之前先找有没有 children，怎么找有没有 children 呢？

思路：封装方法 fn3，从 list 中查找，【谁】的 pid 等于 <font color=e32d40>**item 的 id**</font> 的，就把这个【谁】添加到数组中，最后把整个数组作为添加进去的 item 的 children。

期望结果如下。

```js
// 期望结果
const list = [
    { id: 'a', pid: '', name: '总裁办' },
    { id: 'b', pid: '', name: '行政部' },
    {
        id: 'c',
        pid: '',
        name: '财务部',
        children: [
            { id: 'd', pid: 'c', name: '财务核算部' },
            {
                id: 'e',
                pid: 'c',
                name: '税务管理部',
                children: [
                    { id: 'f', pid: 'e', name: '税务管理 A 部' },
                    { id: 'g', pid: 'e', name: '税务管理 B 部' },
                ],
            },
        ],
    },
]
```

## 5. 递归


