## 5. TreeToArray

```js
const list = [
    { id: 'a', pid: '', name: '总裁办' },
    { id: 'b', pid: '', name: '行政部' },
    {
        id: 'c',
        pid: '',
        name: '财务部',
        children: [
            { id: 'd', pid: 'c', name: '财务核算部' },
            {
                id: 'e',
                pid: 'c',
                name: '税务管理部',
                children: [
                    { id: 'f', pid: 'e', name: '税务管理 A 部' },
                    { id: 'g', pid: 'e', name: '税务管理 B 部' },
                ],
            },
        ],
    },
]

const getTreeToArrayResult = (list) => {
    const arr = []
    const treeToArray = (list) => {
        if (list && list.length > 0) {
            list.forEach((item) => {
                arr.push(item)
                treeToArray(item.children)
                delete item.children
            })
        }
    }
    treeToArray(list)
    return arr
}
const result = getTreeToArrayResult(list)
console.log(result)
```

## 6. 其他写法

### 6.1 array2tree

```js
const list = [
    { id: 'a', pid: '', name: '总裁办' },
    { id: 'b', pid: '', name: '行政部' },
    { id: 'c', pid: '', name: '财务部' },
    { id: 'd', pid: 'c', name: '财务核算部' },
    { id: 'e', pid: 'c', name: '税务管理部' },
    { id: 'f', pid: 'e', name: '税务管理 A 部' },
    { id: 'g', pid: 'e', name: '税务管理 B 部' },
]
const arrayToTree = (list, id = '') => {
  return list.reduce((acc, cur) => {
    if (cur.pid === id) {
      const children = arrayToTree(list, cur.id)
      if (children.length) {
        cur.children = children
      }
      acc.push(cur)
    }
    return acc
  }, [])
}
console.log(arrayToTree(list))
```

### 6.2 tree2array

```js
const list = [
    { id: 'a', pid: '', name: '总裁办' },
    { id: 'b', pid: '', name: '行政部' },
    {
        id: 'c',
        pid: '',
        name: '财务部',
        children: [
            { id: 'd', pid: 'c', name: '财务核算部' },
            {
                id: 'e',
                pid: 'c',
                name: '税务管理部',
                children: [
                    { id: 'f', pid: 'e', name: '税务管理 A 部' },
                    { id: 'g', pid: 'e', name: '税务管理 B 部' },
                ],
            },
        ],
    },
]
const tree2array = (tree) => {
    return tree.reduce((prev, cur) => {
        if (!cur.children) {
            prev.push(cur)
        } else {
            const subList = tree2array(cur.children)
            // 删除 cur 的 children
            delete cur.children
            prev.push(cur, ...subList)
        }
        return prev
    }, [])
}

console.log(tree2array(list))
```

## 7. findId

```js
function getID(json, id, callback) {
    json.some(function (item) {
        if (item.id == id) {
            callback(item)
            return true
        } else if (item.children && item.children.length > 0) {
            getID(item.children, id, callback)
        }
    })
}
getID(list, 'h', function (data) {
    console.log(data)
})
```

## 8. flat

```js
const arr = [1, [2, [3, [4]]]]
function flat(arr, dep = 1) {
    return arr.reduce((acc, cur) => {
        return acc.concat(Array.isArray(cur) && dep > 0 ? flat(cur, dep - 1) : [cur])
    }, [])
}
console.log(flat(arr, 1))

// const test = [1, 2]
// [4] => item
// console.log(test.concat([4])) // [1, 2, 3, 4]
// console.log(test.concat([[4]])) // [1, 2, 3, [4]]
```