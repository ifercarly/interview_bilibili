const list = [
  {id: 'a',pid: '',name: '总裁办'},
  {id: 'b',pid: '',name: '行政部'},
  {id: 'c',pid: '',name: '财务部'},
  {id: 'd',pid: 'c',name: '财务核算部'},
  {id: 'e',pid: 'c',name: '税务管理部'},
  {id: 'f',pid: 'e',name: '税务管理 A 部'},
  {id: 'g',pid: 'e',name: '税务管理 B 部'},
]
const arrayToTree = (array, pid = '') => {
  return array.reduce((prev, cur) => {
    if(cur.pid === pid) {
      const children = arrayToTree(array, cur.id)
      if(children.length) {
        cur.children = children
      }
      prev.push(cur)
    }
    return prev
  }, [])
}
console.log(arrayToTree(list))

