const arr = [1, [2, [3, [4]]]]
function flat(arr, dep = 1) {
  return arr.reduce((acc, cur) => {
    return acc.concat(Array.isArray(cur) && dep > 0 ? flat(cur, dep - 1) : [cur])
  }, [])
}
console.log(flat(arr, 1))


// const test = [1, 2]
// [4] => item
// console.log(test.concat([4])) // [1, 2, 3, 4]
// console.log(test.concat([[4]])) // [1, 2, 3, [4]]