[参考](https://github.com/Advanced-Frontend/Daily-Interview-Question/issues/6)

[参考](https://www.zhihu.com/question/458213150)

## Set

1\. Set 类似于数组，成员唯一且有序的数据结构，没有键名（或者说键值与键名是一致的）。

```js
const s = new Set(['a', 'b', 'c'])
console.log(s.keys()) // 键的集合
console.log(s.values()) // 值的集合
console.log(s.entries()) // 键值对的结合
```

2\. 具有 size 属性可以遍历，遍历方法有 keys、values、entries、forEach，其他方法有：add、delete、has、clear。

## WeakSet

1\. 只接受对象作为值，且此时对象是弱引用的，即如果没有其他的变量或属性引用这个对象，则这个对象将会被垃圾回收掉（不考虑该对象还存在于 WeakSet 中）。

```js
<script>
  const set = new WeakSet()

  const a = { foo: 'hello' }
  set.add(a)

  function test() {
    const b = { bar: 'world' }
    // 把局部变量 b 放入 set 中
    set.add(b)
  }

  test()

  console.log(set) // 打印 set 的值
</script>
```

2\. 没有 size 属性不能遍历，其他方法有 add、delete、has、clear（已废弃）。

## Map

1\. map 类似于对象，和对象不同的是对象本身也可以当做键名。

2\. 具有 size 属性可以遍历，遍历方法有 keys、values、entries、forEach，其他方法有 <font color=e32d40>get</font>、set、has、delete、clear 等，可以跟各种数据格式进行转换。

## WeakMap

1\. 只接受对象作为键名（null 除外），且此时对象是弱引用的。

2\. 没有 size 属性不能遍历，方法有 get、set、has、delete。

