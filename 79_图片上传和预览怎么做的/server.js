const express = require('express');
const cors = require('cors')
const formidable = require('formidable');
const path = require('path')
const app = express();
app.use(express.static(path.join(__dirname, 'upload')))
app.use(cors())

app.post('/api/upload', (req, res, next) => {
  const form = formidable({ multiples: true, keepExtensions: true, uploadDir:  path.join(__dirname, 'upload')});

  form.parse(req, (err, fields, files) => {
    if (err) {
      next(err);
      return;
    }
    res.json({ fields, files });
  });
});

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080 ...');
});