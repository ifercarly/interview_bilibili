## 说一下 new 的执行过程？

```js
function Person(name, age) {
  this.name = name
  this.age = age
}
const p1 = new Person('ifer', 18)
```

1\. 内存中开辟一个空间；

2\. 把 this 指向这个空间；

3\. 执行构造函数中的代码；

4\. 返回 this 实例；

**在构造函数中，如果手动返回了简单数据类型，会忽略，new 出来的结果还是实例。**

```js
function Person(name, age) {
  this.name = name
  this.age = age
  // 如果手动返回了简单数据类型，会忽略
  return 'hello world'
}

const p1 = new Person('ifer', 18)

console.log(p1) // Person { name: 'ifer', age: 18 }
```

**复杂数据类型呢？**

```js
function Person(name, age) {
  this.name = name
  this.age = age

  // new 出来的结果就是这个返回的复杂数据类型
  return { name: 'elser', age: 19 }
}

const p1 = new Person('ifer', 18)

console.log(p1) // { name: 'elser', age: 19 }
```

