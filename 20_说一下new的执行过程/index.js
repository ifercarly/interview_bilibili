function Person(name, age) {
  this.name = name
  this.age = age

  // 如果手动返回了简单数据类型会怎样，new 出来的 p1 还是实例吗
}

const p1 = new Person('ifer', 18)

console.log(p1) // { name: 'elser', age: 19 }
