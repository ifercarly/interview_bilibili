let username = 'qsp'

function foo(username) {
  // 简单数据类型传参传递的是值的拷贝，把这个值的拷贝给了 username 局部变量
  // 所以对这个 username 局部变量的修改当然不会影响外部的
  username = 'zsf'
}
foo(username)

console.log(username)
