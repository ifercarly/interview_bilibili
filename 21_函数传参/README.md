## 函数传参，传递简单数据类型和复杂数据类型有怎样的一个差异？

简单数据类型传递的是值的拷贝，函数内部对参数的修改不会影响外部；

复杂数据类型传递的是引用地址，函数内部对<font color=e32d40>**参数内容**</font>的修改会影响外部，对<font color=e32d40>**参数引用**</font>的修改不会影响外部。

#### 简单数据类型传参

```js
let username = 'qsp'

function foo(username) {
  // 简单数据类型传参传递的是值的拷贝，把这个值的拷贝给了 username 局部变量
  // 所以对这个 username 局部变量的修改当然不会影响外部的
  username = 'zsf'
}
foo(username)

console.log(username) // 'qsp'
```

#### 复杂数据类型传参的 2 种处理情况

修改内容

```js
const obj = {
  name: 'ifer',
}

function foo(obj) {
  // 赋值数据类型传参传递的是引用地址，把这个引用地址拷贝给了 obj 局部变量，而这个 obj 引用地址指向的还是曾经的那个空间
  // 所以对这个 obj 局部变量【内容的修改】当然会影响外部的
  obj.name = 'elser'
}
foo(obj)

console.log(obj) // 'elser'
```

修改引用地址

```js
let obj = {
  name: 'ifer',
}

function foo(obj) {
  // 赋值数据类型传参传递的是引用地址，把这个引用地址拷贝给了 obj 局部变量，而这个 obj 引用地址指向的还是曾经的那个空间
  // 所以对这个 obj 局部变量【引用的修改】当然不会影响外部的
  obj = {
    name: 'elser',
  }
}
foo(obj)

console.log(obj) // 'ifer'
```
