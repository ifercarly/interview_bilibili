## 小程序支付流程

1\. 支付前，前端要确保是登录的状态，并且后续和支付相关的接口操作都要携带 token 到后端。

2\. 前端传递相关参数（拿电商平台来说，参数至少包含 skuId 以及对应的数量、收获地址等信息）到后端，后端返回<font color=e32d40>**订单编号**</font>。

3\. 前端根据订单编号调用后端提供的<font color=e32d40>**预支付**</font>的接口，目的是为了<font color=e32d40>**拿到发起微信支付时候的相关参数**</font>，一般包含如下信息。

[参数说明](https://developers.weixin.qq.com/miniprogram/dev/api/payment/wx.requestPayment.html)

```js
const payInfo = {
  nonceStr: "M4agEIfBFApG1AFH", # 随机字符串，长度为 32 个字符以下
	package: "prepay_id=wx12215548926387c51e4005942e88800000", # 预支付 ID
	paySign: "BBBD1BDB79EA3DF535F699DB6659F943", # 签名
	signType: "MD5", # 签名算法
	timeStamp: "1673531750" # 时间戳
}
```

4\. 前端调用 `wx.requestPayment(payInfo)` 发起支付。

5\. 微信的后台会把这次支付的结果通知到我们的后台，后台更新订单状态。

6\. 前端根据订单编号再次调用后端的接口查询支付结果并进行展示。







