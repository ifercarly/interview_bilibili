# 说一下图片懒加载的原理

如果图片进入了可视区，就把图片上装地址的某个属性值给图片真正的 src 属性。

```html
<img src="" data-img="https://www.itcast.cn/20224109184146299.jpg" alt="" />
```

关键是如何判断图片是否进入可视区呢？

## 位置计算

如果【图片顶部距离窗口数值】小于等于【可视区文档的高度】，说明进入了可视区。

<img src="README.assets/image-20220717215437408.png" alt="image-20220717215437408" style="zoom: 50%;" />

```html
<div class="imgBox">
  <img src="" data-img="https://www.itcast.cn/images/newslide/homepageandphone/20224109184146299.jpg" alt="" />
</div>
<script>
  const oImg = document.querySelector('img')

  window.addEventListener('scroll', function () {
    // 图片顶部距离窗口数值
    // 可视区文档的高度
    if (oImg.getBoundingClientRect().top <= document.documentElement.clientHeight) oImg.src = oImg.dataset.img
  })
</script>
```

## IntersectionObserver

通过浏览器提供的 `IntersectionObserver` 对象创建一个实例，调用实例的 observe 方法可以观测某个 img 元素；

在 IntersectionObserver 的参数回调里面可以通过 `isIntersecting` 属性来判断这个 img 元素是否进入可视区；

如果进入就把 img 元素上装地址的某个属性给图片真正的 src 属性。

```html
<div class="imgBox">
  <img src="" data-img="https://www.itcast.cn/images/newslide/homepageandphone/20224109184146299.jpg" alt="" />
</div>
<script>
  const oImg = document.querySelector('img')

  const observer = new IntersectionObserver(([{ isIntersecting }]) => {
    if (isIntersecting) {
      oImg.src = oImg.dataset.img
      observer.unobserve(oImg)
    }
  })
  observer.observe(oImg)
</script>
```
