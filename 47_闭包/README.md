# 说一下你对闭包的理解？

## 概念

闭包（Closure）：有人说闭包是一种**现象**，MDN 上面说闭包是一个**函数**。

<font color=e32d40>**背**</font>：【一个函数】使用了其【外部函数】中的局部变量，使用变量的地方我们称为发生了【闭包现象】，变量定义所在的函数我们称为【闭包函数】。



```js
function foo() {
  let age = 18
  return function () {
    console.log(age + 1)
  }
}
const bar = foo()
bar()
```



思考：下面代码有没有闭包产生？



```js
let age = 18
function foo() {
  console.log(age + 1)
}
foo()
```





```js
;(function () {
  let age = 18
  function foo() {
    console.log(age + 1)
  }
  foo()
})()
```





## 特性

普通函数调用完毕，内部局部变量马上销毁。

```js
function foo() {
  let age = 18
}
foo()
```

闭包函数调用完毕，会使内部形成这个【闭包函数】的变量（age）常驻内存，所以<font color=e32d40>**滥用**</font>闭包会造成内存浪费。

```js
function foo() {
  let age = 18
  return function () {
    age += 1
    console.log(age)
  }
}
const bar = foo() // foo 中的 age 在内存中没有释放
bar() // 19
bar() // 20
```



## 你在开发中哪些地方用到过闭包吗？





































我在开发中无时无刻，或者不经意间就会用到闭包，因为只要符合了上面说的概念，就是使用了闭包。

```html
<!-- 例如在早期，给一组按钮循环绑定点击事件，点击的时候期望输出对应的索引（用 var 定义的索引变量）。 -->

<!-- 其实是做不到的，因为当点击按钮的时候，for 循环早就走完了，i 也就变成了最终的那个数值，输出的结果也永远是最终的那个数值。 -->
<body>
  <button>1</button>
  <button>2</button>
  <button>3</button>
  <button>4</button>
  <button>5</button>
  <script>
    const aBtn = document.querySelectorAll('button')
    for (var i = 0; i < aBtn.length; i++) {
      aBtn[i].onclick = function () {
        console.log(i)
      }
    }
  </script>
</body>
```

这个时候可以每次循环都产生一个闭包函数，并把循环时候的变量 i 传递过去，这个**循环时候的 i **就会常驻内存，就做到了咱内部使用的 i 就是循环时候那一刻的 i。

```html
<body>
  <button>1</button>
  <button>2</button>
  <button>3</button>
  <button>4</button>
  <button>5</button>
  <script>
    const aBtn = document.querySelectorAll('button')
    for (var i = 0; i < aBtn.length; i++) {
      ;(function (i) {
        // 【函数形参 i 也是局部变量】
        aBtn[i].onclick = function () {
          console.log(i)
        }
      })(i)
    }
  </script>
</body>
```
