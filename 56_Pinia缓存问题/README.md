## Pinia / Vuex / Redux / ... 缓存问题

## 知识铺垫

`1369155859933827074`

1\. 存储到 Pinia 中的数据，刷新后才会丢失（正常的组件销毁或路由跳转并不会影响到它）。

2\. 组件一旦被渲染完毕，后续数据的变化（Pinia 或自身组件中的数据等）并不会使此组件初始化相关的钩子（例如 created）重新执行（模板中才会根据数据的变化实时进行渲染），即便这个钩子中用到了这个变化的数据，也就意味着如果你想依赖数据的变化重新执行初始化钩子内部的代码逻辑是做不到的。















## 问题展示



































## 问题分析



初始渲染（第一次渲染组件或刷新页面时） Goods 组件为什么是正常的？



下一次返回到 Goods 组件时，按钮的状态为什么先禁用再启用（不正常）？



































## 处理办法

导致这个 Bug 的原因就是因为 Pinia 中有残留数据，导致 GoodsSku 组件的模板渲染了 2 次，而第二次模板的渲染并没有经过 setup 中的处理逻辑，从而覆盖了第一次正确的渲染结果。

1\. 父组件/子组件组件销毁时清理 Pinia 中缓存的数据（最开始初始化时清空也行）。

<font color=e32d40>**保证子组件只渲染第一次。**</font>

```js
onBeforeUnmount(() => {
	store.goods.clearGoodsInfo()
})
```

2\. 把数据缓存在存储在组件本身中。

```js
import { GoodsInfo as GoodsInfoType } from '@/types/goods'
const info = ref({} as GoodsInfoType)
watchEffect(async () => {
	const goodsId = route.params.id
  	if (route.fullPath === `/goods/${goodsId}`) {
    	const r = await request.get<IResponse<GoodsInfoType>>('/goods', {
      		params: {
        		id: goodsId,
      		},
    	})
    	info.value = r.data.result
  	}
})
```

3\. 监听数据更新。

```js
// store.goods.info // 时机太早了
watch(() => props.goods, () => {
	updateDisabledStatus(props.goods.specs, pathMap)
})
```









## 问题总结

一旦把数据存储到了全局（Pinia、Vuex 等），并且在组件模板中使用到这个数据的时候，可能会使组件渲染时机提前或模板重复渲染，从而造成数据展示不准确的问题，建议通过 watch 监听全局数据的变化，根据变化后的新数据从新执行 setup 时的业务逻辑。



















