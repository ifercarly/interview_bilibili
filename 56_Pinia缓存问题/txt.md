【问题说明】

现在我们看到的有两个关键的组件，分别是商品详情（Goods）和商品规格（GoodsSku），他们两者之间的关系是，商品详情组件引入了商品规格组件。

需要大家注意的是，商品规格中每一个按钮的禁用与否是需要前端在 GoodsSku 组件<font color=e32d40>**初始化的时候**</font>计算而来的，例如现在看到的 20cm 禁用状态就是计算出来的结果，是正常的。


当我们点击其他路由地址跳转到对应的组件，再次点击返回按钮的时候，会发现曾经**应该有的**禁用状态却不见了，这并不是我们期望的。

如果再细心观察的话，其实它会有一个先禁用然后再恢复默认的启用状态的过程，咱们再次演示一下。

那么为什么会这样呢？


【问题分析】

1\. 开始渲染 Goods 组件的时候，会执行 setup 中的代码和模板渲染。

2\. 首先从 Pinia 中获取了空对象 info，紧接着调用 goods.getGoodsInfo action 发起请求，注意这个过程是异步的。

3\. 然后渲染模板，碰到 v-if="info.categories"，由于 info 是个空对象，所以并不会渲染内部的 GoodsSku 组件。

4\. 等一会 action 中后端接口的**初始数据**返回，接下来把这个初始数据赋值给了 info。

5\. info 一般被赋值，数据发生变化，Goods 组件的<font color=e32d40>**模板**</font>中用到 info 的地方会自动渲染，此时 v-if="info.categories" 条件成立，渲染 GoodsSku 组件并传递初始数据 info。

6\. 执行 GoodsSku 组件 setup 中的代码，通过 updateDisabledStatus 函数更新 info 中的数据（Pinia 中的 info 也会跟着变化），主要更新的就是按钮禁用启用状态相关的数据，接下来来渲染模板，按钮得到禁用。


-----------------------------------------

1\. 点击按钮跳转到其他路由组件，前面的 Goods 组件确实会被销毁，但注意此时 Pinia 中的 info 数据还是在的（并且是处理好的）。

2\. 点击返回按钮的时候，开始重新创建 Goods 组件，同样会执行 setup 中的代码和模板渲染。

3\. 首先从 Pinia 中获取 info（注意此时的 info 是上次处理好的完整的一份数据），紧接着调用 goods.getGoodsInfo action 发起请求，注意这个过程是异步的。

4\. 然后渲染模板，碰到 v-if="info.categories"，此时条件成立，渲染 GoodsSku 组件并传递 info 数据。

5\. 执行 GoodsSku 组件 setup 中的代码，通过 updateDisabledStatus 函数更新 info 中的数据（其实此时不更新也没关系，因为已经计算好了），渲染模板，【按钮得到禁用】。

注意！！！

等一会 action 中后端接口的**初始数据**返回，接下来又把这个初始数据赋值给了 info

info 发生变化，goods 组件的模板中用到 info 的地方会自动更新，此时 v-if="info.categories"> 成立，渲染 GoodsSku 组件并传递初始数据 info 。

GoodsSku 的模板中再次根据初始数据进行渲染，【按钮禁用状态丢失】，因为这一次的初始数据并没有经过 updateDisabledStatus 的计算。