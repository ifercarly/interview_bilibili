# computed、methods、watch 之间的区别

computed：基于依赖（data 中的数据）进行<font color=e32d40>**缓存**</font>，会产生一个新数据，一般内部不会写异步代码。

```js
export default {
  name: 'App',
  data() {
    return {
      firstName: 'foo',
      lastName: 'bar',
    }
  },
  computed: {
    fullName() {
      return this.firstName + ' ' + this.lastName
    },
  },
}
```

methods 也可以基于依赖产生一个新数据，但<font color=e32d40>**不具有缓存**</font>。

```js
export default {
  name: 'App',
  data() {
    return {
      firstName: 'foo',
      lastName: 'bar',
    }
  },
  methods: {
    fn() {
      return this.firstName + ' ' + this.lastName
    },
  },
}
```

watch 和上面二者的区别是应用场景会不一样，它一般是用来监听 data 中某一个数据的变化做一些副作用相关的操作，例如发请求。

```js
export default {
  name: 'App',
  data() {
    return {
      firstName: 'foo',
      lastName: 'bar',
    }
  },
  watch: {
    firstName(newValue) {
      axios.get('/hello', { params: { content: newValue } })
    },
  },
}
```

watch 也可以监听路由，有个时候可以利用这种写法来解决路由缓存问题。

```js
export default {
  name: 'App',
  watch: {
    $route(to, from) {
      
    },
  },
}
```

