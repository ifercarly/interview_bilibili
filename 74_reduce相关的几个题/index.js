// 'a.b.c' => ['a', 'b', 'c'] => ['c', 'b', 'a'] => a: { b: c }
const str = 'a.b.c'

/* const r = str.split('.').reverse().reduce((acc, cur) => {
  // ['c', 'b', 'a']
  const obj = {}
  // 没有传初始值
  // acc => 上一项
  // cur => 下一项
  obj[cur] = acc
  return {
    [cur]: acc
  }
})
console.log(r) */

const r = str.split('.').reverse().reduce((acc, cur) => {
  return {
    [cur]: acc
  }
}, {})
console.log(r)