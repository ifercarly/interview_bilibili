# Vue3 中的 watch

## 问题

```vue
<script setup lang="ts">
import { watch, ref } from 'vue'
const obj = ref({
  eat: '西瓜',
})
watch(obj, (newValue) => {
  console.log(newValue)
})
</script>

<template>
  <p>{{ obj.eat }}</p>
  <button @click="obj.eat = '面条'"><code>obj.eat = '面条'</code></button>
</template>
```

## 解决 1：修改第 1 层

因为 watch 对 ref 数据的监听默认是<font color=e32d40>**浅监听的**</font>，既然是浅监听，那可以直接修改第一层就好啦，也就是 `obj.value`。

```vue
<template>
  <button @click="obj = { eat: '面条' }"><code>obj = { eat: '面条' }</code></button>
</template>
```

## 解决 2：开启深度监听

```vue
<script setup lang="ts">
import { watch, ref } from 'vue'
const obj = ref({
  eat: '西瓜',
})
watch(
  obj,
  (newValue) => {
    console.log(newValue) // Proxy { eat: '面条' }
  },
  // #2
  { deep: true }
)
</script>

<template>
  <p>{{ obj.eat }}</p>
  <!-- #1 -->
  <button @click="obj.eat = '面条'"><code>obj.eat = '面条'</code></button>
</template>
```

## 解决 3：监听 reactive

`obj.value` 是一个 reactive 数据，而 reactive 数据是开启深度监听的，并且不能修改。

```vue
<script setup lang="ts">
import { watch, ref } from 'vue'
const obj = ref({
  eat: '西瓜',
})
watch(obj.value, (newValue) => {
  console.log(newValue) // Proxy { eat: '面条' }
})
</script>

<template>
  <p>{{ obj.eat }}</p>
  <button @click="obj.eat = '面条'"><code>修改 obj.eat</code></button>
</template>
```

🤔 注意：直接监听 reactive，对 reactive 本身的修改是不会触发监听的。

```html
<button @click="obj = { eat: '面条' }"><code>obj = { eat: '面条' }</code></button>
```

## 解决 4：watch 指定函数，返回 ref

任何情况都不会触发监听，除非开启 deep。

```html
<script setup lang="ts">
import { watch, ref } from 'vue'
const obj = ref({
  eat: '西瓜',
})
watch(
  () => obj,
  (newValue) => {
    console.log(newValue)
  }
)
</script>

<template>
  <p>{{ obj.eat }}</p>
  <button @click="obj.eat = '面条'"><code>obj.eat = '面条'</code></button>
  <button @click="obj = { eat: '面条' }"><code>obj = { eat: '馒头' }</code></button>
</template>
```

## 解决 5：watch 指定函数，返回 reactive（ref 中的对象）

对这个 reactive 本身的修改会触发监听，内部数据的变化则不会。<font color=e32d40>开启 deep 后上面 2 种情况都会被触发监听</font>。

```html
<script setup lang="ts">
import { watch, ref } from 'vue'
const obj = ref({
  eat: '西瓜',
})
watch(
  () => obj.value,
  (newValue) => {
    console.log(newValue)
  }
)
</script>

<template>
  <p>{{ obj.eat }}</p>
  <button @click="obj.eat = '面条'"><code>obj.eat = '面条'</code></button>
  <button @click="obj = { eat: '馒头' }"><code>obj = { eat: '馒头' }</code></button>
</template>
```

## 解决 6：watch 指定函数，返回 reactive（直接定义的对象）

对这个 reactive 本身的修改和内部数据的变化都不会触发监听。<font color=e32d40>开启 deep 后第 2 种情况会被触发监听</font>。

## 解决 7：watch 指定函数，返回普通值

任何影响到此值的修改并且是响应式的，都会触发监听。

```html
<script setup lang="ts">
import { watch, ref } from 'vue'
const obj = ref({
  eat: '西瓜',
})
watch(
  () => obj.value.eat,
  (newValue) => {
    console.log(newValue)
  }
)
</script>

<template>
  <p>{{ obj.eat }}</p>
  <button @click="obj.eat = '面条'"><code>obj.eat = '面条'</code></button>
  <button @click="obj = { eat: '馒头' }"><code>obj = { eat: '馒头' }</code></button>
</template>
```

