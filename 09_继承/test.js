function Person(name, age) {
  this.name = name
  this.age = age
}
Person.prototype.say = function () {
  return 'Hello World'
}

function Star(name, age) {
  Person.call(this, name, age)
}

if (typeof Object.create !== 'function') {
  Object.create = function (o) {
    function F() {}
    F.prototype = o
    return new F()
  }
}
Star.prototype = Object.create(Person.prototype)
Star.prototype.constructor = Star

const s1 = new Star('ifer', 18)
console.log(s1.say())
