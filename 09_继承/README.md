## 盗用构造函数继承

```js
function Person(name, age) {
  this.name = name
  this.age = age
  this.say = function () {
    return 'Hello World'
  }
}

function Star(name, age) {
  Person.call(this, name, age)
}

const s = new Star('ifer', 18)
console.log(s.name, s.age)
s.say()
```

## 盗用构造函数继承的问题

```js
function Person(name, age) {
  this.name = name
  this.age = age
  this.say = function () {
    return 'Hello World'
  }
}

function Star(name, age) {
  Person.call(this, name, age)
}

const s1 = new Star('ifer', 18)
const s2 = new Star('elser', 18)

// s1.say 和 s2.say 做的事情是一样的，却发现不相等，内存浪费
console.log(s1.say === s2.say) // false
```

## 错误的想法

```js
function Person(name, age) {
  this.name = name
  this.age = age
}
// #1 挂载到原型
Person.prototype.say = function () {
  return 'Hello World'
}

function Star(name, age) {
  Person.call(this, name, age)
}

// #2 把 Person.prototype 给 Star.prototype
Star.prototype = Person.prototype

const s1 = new Star('ifer', 18)
const s2 = new Star('elser', 18)

console.log(s1.say === s2.say)
```

错误原因

```js
function Person(name, age) {
  this.name = name
  this.age = age
}
Person.prototype.say = function () {
  return 'Hello World'
}

function Star(name, age) {
  Person.call(this, name, age)
}

Star.prototype = Person.prototype
Star.prototype.startParty = function () {
  return 'Party!'
}

const p1 = new Person('ifer', 18)
// 问题：儿子构造函数挂载的方法影响到了父类及对应的实例，这并不是我们期望的
console.log(p1.startParty())
```

## 原型链继承

```js
function Person(name, age) {
  this.name = name
  this.age = age
}
Person.prototype.say = function () {
  return 'Hello World'
}

function Star(name, age) {
  Person.call(this, name, age)
}

const p = new Person()
Star.prototype = p
Star.prototype.startParty = function () {
  return 'Party!'
}

const s1 = new Star('ifer', 18)
console.log(s1.say())
```

上面代码的问题

```JS
function Person(name, age) {
  this.name = name
  this.age = age
}
Person.prototype.say = function () {
  return 'Hello World'
}

function Star(name, age) {
  Person.call(this, name, age)
}

const p = new Person()
Star.prototype = p
Star.prototype.startParty = function () {
  return 'Party!'
}

const s1 = new Star('ifer', 18)
console.log(Star.prototype.constructor === Star) // false
console.log(s1.constructor === Star) // 类型不正确
```

解决类型不正确的问题

```js
function Person(name, age) {
  this.name = name
  this.age = age
}
Person.prototype.say = function () {
  return 'Hello World'
}

function Star(name, age) {
  Person.call(this, name, age)
}

const p = new Person()
Star.prototype = p
// #mark
Star.prototype.constructor = Star
Star.prototype.startParty = function () {
  return 'Party!'
}

const s1 = new Star('ifer', 18)
console.log(Star.prototype.constructor === Star) // false
console.log(s1.constructor === Star) // 类型不正确
```

## 组合继承

组合继承：盗用构造函数继承 + 原型链继承

## 组合继承的问题

效率问题：就是父类构造函数始终会被调用两次；子类原型上有冗余的属性。

## 原型式继承

2006, Douglas Crockford, [Prototypal Inheritance in JavaScript](https://www.crockford.com/javascript/prototypal.html)

这个 `object()` 函数会创建一个临时构造函数，将传入的对象赋值给这个构造函数的原型对象，然后返回这个临时构造函数的一个实例。本质上，`object()` 是对传入的对象执行了一次浅复制。

```js
function object(o) {
  function F() {}
  F.prototype = o
  return new F()
}
```

```js
function Person(name, age) {
  this.name = name
  this.age = age
}
Person.prototype.say = function () {
  return 'Hello World'
}

function Star(name, age) {
  Person.call(this, name, age)
}

// #1
function object(o) {
  function F() {}
  F.prototype = o
  return new F()
}
// #2
Star.prototype = object(Person.prototype)
Star.prototype.constructor = Star

const s1 = new Star('ifer', 18)
console.log(s1.say())
```

ECMAScript 5 通过增加 `Object.create()` 方法将原型式继承的概念规范化。

```js
function Person(name, age) {
  this.name = name
  this.age = age
}
Person.prototype.say = function () {
  return 'Hello World'
}

function Star(name, age) {
  Person.call(this, name, age)
}

if (typeof Object.create !== 'function') {
  Object.create = function (o) {
    function F() {}
    F.prototype = o
    return new F()
  }
}
Star.prototype = Object.create(Person.prototype)
Star.prototype.constructor = Star

const s1 = new Star('ifer', 18)
console.log(s1.say())
```

## 寄生式继承

寄生式继承背后的思路类似于寄生构造函数和工厂模式：创建一个实现继承的函数，以某种方式增强对象，然后返回这个对象。
通过寄生式继承给对象添加函数会导致函数难以重用，与构造函数模式类似。

## 寄生式组合继承
