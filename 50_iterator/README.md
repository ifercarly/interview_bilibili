```html
<script>
  const obj = {
    a: 4,
    b: 7,
    // 迭代接口/迭代协议

    // 目的：为所有数据结构提供一种统一的遍历机制，即 for/of 循环。

    // 概念：本质上是一个具有特定规范**方法**，这个方法要返回一个具有 next 方法的对象（又称为迭代器对象），每次调用 next 方法又可以拿到一个对象，这个对象的 value 就是数据结构中对应的值。

    // for/of 循环消费的就是默认的迭代接口 `[Symbol.iterator]`，它的执行过程如下。
    // 1. 自动调用此迭代接口。
    // 2. 得到迭代器对象后依次调用 next 方法得到对象。
    // 3. 把对象的 value 作为 for/of 循环时候的 v。
    [Symbol.iterator]() {
      let startIndex = 0
      const arr = Object.values(this)
      return {
        next() {
          if (startIndex < arr.length) {
            return {
              value: arr[startIndex++],
              done: false,
            }
          } else {
            return {
              value: undefined,
              done: true,
            }
          }
        },
      }
    },
  }

  for (let v of obj) {
    console.log(v)
  }
</script>
```
