# 父传子都有哪些方法，注意点是什么？

## 1. 父亲通过自定义属性传递，儿子通过 defineProps 接收

`Parent.vue`

```vue
<script setup lang="ts" name="Parent">
import { ref } from 'vue'
import Child from './Child.vue'

const count = ref(6)
</script>

<template>
  <div>
    Parent: {{count}}
    <hr />
    <!-- #1 父亲通过自定义属性传递 -->
    <Child :count="count" />
  </div>
</template>
```

`Child.vue`

```html
<script setup lang="ts" name="Child">
// #2 儿子通过 defineProps 接收
defineProps<{
  count: number
}>()
</script>

<template>
  <div>Child: {{ count }}</div>
</template>
```

## 2. v-model

`Parent`

```vue
<script setup lang="ts" name="Parent">
import { ref } from 'vue'
import Child from './Child.vue'

const count = ref(6)
</script>

<template>
  <div>
    Parent: {{count}}
    <hr />
    <!-- <Child :modelValue="count" @update:modelValue="count = $event" /> -->
    <Child v-model="count" />
  </div>
</template>
```

`Child.vue`

```vue
<script setup lang="ts" name="Child">
defineProps<{
  modelValue: number
}>()
</script>

<template>
  <div>
    <div>Child: {{ modelValue }}</div>
    <button @click="$emit('update:modelValue', 888)">update:ModelValue</button>
  </div>
</template>
```

## 3. 父亲通过自定义属性传递，儿子通过 $attrs 接收

$attrs 是非 props 属性组成的对象。

`Parent.vue`

```vue
<script setup lang="ts" name="Parent">
import { ref } from 'vue'
import Child from './Child.vue'

const count = ref(6)
</script>

<template>
  <div>
    Parent: {{count}}
    <hr />
    <!-- #1 父亲通过自定义属性传递 -->
    <Child :count="count" />
  </div>
</template>
```

`Child.vue`

```vue
<template>
  <!-- #2 儿子通过 $attrs 接收 -->
  <div>Child: {{ $attrs.count }}</div>
</template>
```

## 4. 父亲通过 ref 获取子组件实例，...

`Parent.vue`

```vue
<script setup lang="ts" name="Parent">
import { ref } from 'vue'
import Child from './Child.vue'

const count = ref(6)

// #1
const childCmp = ref<InstanceType<typeof Child> | null>(null)

const handleClick = () => {
  // #3 直接修改子组件实例上的值为父组件的数据
  childCmp.value!.count = count.value
  // 或调用子组件方法的同时并把父组件的数据传过去，子组件内部进行修改
  // childCmp.value?.changeCount(count.value)
}
</script>

<template>
  <div>
    Parent: {{count}}
    <button @click="handleClick">click</button>
    <hr />
    <!-- #2 -->
    <Child ref="childCmp" />
  </div>
</template>
```

`Child.vue`

```vue
<script setup lang="ts" name="Child">
import { ref } from 'vue'

const count = ref(1)

const changeCount = (value: number) => {
  count.value = value
}

defineExpose({
  count,
  changeCount,
})
</script>

<template>
  <div>Child: {{ count }}</div>
</template>
```

## 5. $children

Vue2 也可以通过 $children 获取子组件实例，Vue3 废弃了。

## 6. slot

`Parent.vue`

```html
<script setup lang="ts" name="Parent">
import { ref } from 'vue'
import Child from './Child.vue'

const count = ref(6)
</script>

<template>
  <div>
    Parent: {{count}}
    <hr />
    <Child>{{ count }}</Child>
  </div>
</template>
```

`Child.vue`

```html
<template>
  <div>Child: <slot /></div>
</template>
```

## 7. ...



# 注意点

如果传递过来的是一个简单数据类型，子组件一定不能改，如果是一个复杂数据类型，内容可以改，引用不能改，即便引用可以改，也不建议改，因为单项数据流。

