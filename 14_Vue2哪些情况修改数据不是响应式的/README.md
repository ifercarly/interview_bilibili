## Vue2 哪些情况对数据的操作不是响应式的？

1\. 给对象后续新增的 key 进行赋值；

```js
data() {
    return {
        obj: {}
    }
}
this.obj.age = 18
```

2\. 删除对象中的某一项；

```js
data() {
    return {
        obj: {
            age: 18
        }
    }
}
delete this.obj.age
```



3\. 通过索引去修改数组的元素；

```js
data() {
    return {
        arr: ['a']
    }
}
this.arr[0] = 'b'
```



## 为什么这些情况不是响应式的？

1\. 给对象后续新增的 key 进行赋值；

原因：因为 `Object.defineProperty()` 是递归劫持的对象中的每一个<font color=e32d40>**属性**</font>，如果这个属性是后续被添加的，当然就没有被劫持到。

2\. 删除对象中的某一项；

原因：`Object.defineProperty()` 不支持对删除操作的劫持。

3\. 通过索引去修改数组的元素；

原因：并不是 `Object.defineProperty()` 不支持对数组的劫持，而是作者出于性能的原因没有这样做。

## 怎么解决？

1\. 给对象后续新增的 key 进行赋值；

解决：`Vue.set()` 或 `this.$set()`

2\. 删除对象中的某一项；

解决：`Vue.delete()` 或 `this.$delete()`

3\. 通过索引去修改数组的元素；

解决：`Vue.set()` 或 `this.$set()` 或 `arr.splice()`

## Vue3 呢？

1\. 给对象后续新增的 key 进行赋值；

Vue3 响应式的原理换成了 Proxy，可以直接代理整个**对象**，就无所谓这个属性是不是后续添加的了。

2\. 删除对象中的某一项；

Proxy 支持对删除操作的拦截。

3\. 通过索引去修改数组的元素；

Proxy 对数组的操作不存在性能问题。
