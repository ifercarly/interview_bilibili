# 说一下你对 Vuex 的理解？

## 是什么？

Vuex 是一个全局的状态管理库。

## 怎么用？

<font color=e32d40>**配置项**</font>

一般使用的时候它里面有 state、mutation、action、getters、modules、plugins 等配置项。

<font color=e32d40>**触发流程**</font>

需求：例如点击按钮发请求，需要把请求的结果存储到 Vuex。

视图 => dispatch => action => 异步请求并拿到结果 => commit => mutation => 修改 state => 视图改变。

[链接](https://vuex.vuejs.org/zh/)

## 解决了什么问题？

解决了非关系型组件间数据的传递和共享问题。

## 有没有替代方案？

不过呢？有的时候组件间关系非常明确的时候，我会使用 props、ref、EventBus、Provide 等方法。

## 衍生问题

**mutation 里面为什么建议只写同步代码去修改 state？写异步会怎样？**

写同步代码可以保证，mutation 执行完毕后可以得到一个确定的状态，方便 devtools 打个快照存下来，实现 `time-travel` 调试（在调试工具中能根据触发 mutation 的时间来进行切换和调试）。

如果 mutation 里面写异步代码，严格模式下会有警告，因为不好追踪状态是何时变更的，给调试带来困难。

