## 说一下 Less 你用过哪些特性（除了变量、嵌套、计算，再找几条）？



继承、混入、函数、循环等。



## Extend 继承

`before`

```less
// #1 定义一个普通的 class
.box-shadow {
  box-shadow: 0 0 9px 3px #ccc;
  border-radius: 6px;
}

.box1 {
  height: 200px;
  width: 400px;
  margin: 50px auto 0;
  // #2 使用
  &:extend(.box-shadow);
}

.box2 {
  height: 100px;
  width: 400px;
  margin: 30px auto 0;
  &:extend(.box-shadow);
}
```

<font color=e32d40>🍜 **特点：编译后的 CSS 会保留 .box-shadow，会把 .box1、.box2 和 .box-shadow 抽离到一起。**</font>

`after`

```css
.box-shadow,
.box1,
.box2 {
  box-shadow: 0 0 9px 3px #ccc;
  border-radius: 6px;
}
.box1 {
  height: 200px;
  width: 400px;
  margin: 50px auto 0;
}
.box2 {
  height: 100px;
  width: 400px;
  margin: 30px auto 0;
}
```

## Mixin 混入（类似于自定义函数）

`before`

```less
// #1 如果这儿带了括号，此 .box-shadow 不会生成
.box-shadow(@radius: 6px) {
  box-shadow: 0 0 9px 3px #ccc;
  border-radius: @radius;
}

.box1 {
  height: 200px;
  width: 400px;
  margin: 50px auto 0;
  .box-shadow();
}

.box2 {
  height: 100px;
  width: 400px;
  margin: 30px auto 0;
  // #2 可以传递参数
  .box-shadow(50px);
}
```

<font color=e32d40>🍜 **特点：编译后的 CSS 不会保留 .box-shadow，不会把 .box1、.box2 和 .box-shadow 抽离到一起。**</font>

`after`

```css
.box1 {
  height: 200px;
  width: 400px;
  margin: 50px auto 0;
  box-shadow: 0 0 9px 3px #ccc;
  border-radius: 6px;
}
.box2 {
  height: 100px;
  width: 400px;
  margin: 30px auto 0;
  box-shadow: 0 0 9px 3px #ccc;
  border-radius: 50px;
}
```

## 内置函数

```less
.box1 {
  height: 200px;
  width: 400px;
  margin: 50px auto 0;
  // return a color which is 10% *lighter* than @color
  background-color: lighten(red, 20%);
}
```

## 循环

Less 本省没有循环，但是函数可以递归调用，这样就可以模拟循环。

实际项目中，会提前定义一些公共样式为了方便直接使用，例如希望定义一些不同字体大小的 class，但是一个个写太麻烦。

```css
.f12 {
  font-size: 12px;
}
.f14 {
  font-size: 14px;
}
.f16 {
  font-size: 16px;
}
.f18 {
  font-size: 18px;
}
.f20 {
  font-size: 20px;
}
.f22 {
  font-size: 22px;
}
.f24 {
  font-size: 24px;
}
```

```less
.for(@i) when(@i <= 24) {
  .f@{i} {
    font-size: @i + 0px;
  }
  .for((@i + 2));
}
.for(12);
```

