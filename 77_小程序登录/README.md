## 问：小程序登录怎么做的？



1\. 前端通过调用 `wx.login()` 拿到 code。



2\. 前端通过调用 `wx.request()` 发送 code 到后端。



3\. 后端调用 `code2Session()` 方法并传递 appid（小程序的id）、appsecret（小程序秘钥）、code 等参数到微信的后台。



4\. 微信的后台返回 session_key（当前我们公司的后端和微信后端通信时的秘钥） 和 openid（用户的唯一标识）到我们的后台。



5\. 我们的后端生成 token（并把 token 和 openid 进行关联，说白了通过 token 就能知道是哪个用户），并把 token 返回到小程序端。



6\. 前端存储 token 到本地，后续的所有请求都带上这个 token 到后端。





