# 说一下 v-show 和 v-if 的异同

**相同**

两者都是用来控制元素可见不可见的。

**不同**

v-if 是通过 JS 来控制元素的<font color="e32d40">**创建和销毁**</font>，v-show 是通过 CSS 来控制元素的<font color=e32d40>**显示和隐藏**</font>。

v-if 有较高的切换开销。

v-show 有较高的初始渲染开销。

```html
<div v-if="false">
    
</div>
```



**使用**

一般需要频繁切换的元素建议 v-show，需要多个分支条件处理的元素使用 v-if 比较合适。
