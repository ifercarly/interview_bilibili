## 问题：如何实现一个盒子水平/垂直居中？

### 方法 1

父元素设置 `display: flex;` 然后通过 `justify-content: center;` 进行主轴居中，`align-items: center;` 进行侧轴居中。

### 方法 2

父元素设置相对定位，子元素设置绝对定位，然后...

```CSS
.inner {
  width: 100px;
  height: 100px;
  position: absolute;
  top: 50%;
  left: 50%;
  margin-top: -50px;
  margin-left: -50px;
}
```

注意点：这儿的 margin 往往不要用百分比，因为 margin 使用百分比是相对于其父盒子的宽度的，[CSS 百分比都是相对于谁](https://www.zhihu.com/question/36079531/answer/65809167)。

### 方法 3

上面的方案，margin 进行位移的缺点是：需要明确知道自身元素的宽高。

有时候在不知道盒子自身宽高的情况下推荐使用 `transform: translate(-50%, -50%)` 进行位移，因为 translate 的百分比是相对于盒子自身的，这正是我们期望的。
