# null 和 undefined 的表现形式有什么区别

## typeof 的表现

```js
typeof null // 'object'
typeof undefined // 'undefined'
```

## 和数字相加的表现

```js
null + 4 // 4
undefined + 7 // NaN

// Number(null) // 0
// Number(undefined) // NaN
```

## JSON.stringify

```js
JSON.stringify({ foo: undefined, bar: null }) // '{bar: null}'
```

## ...

