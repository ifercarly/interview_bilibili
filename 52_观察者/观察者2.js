class Dep {
  constructor() {
    this.subs = []
    // #1
    this.state = 36
  }
  addSub(sub) {
    if (sub && sub.update) {
      this.subs.push(sub)
    }
  }
  notify() {
    // #3 把可以把整个目标传递给观察者，观察者可以拿到最新的状态
    this.subs.forEach((sub) => sub.update(this))
  }
  // #2
  setState(state) {
    this.state = state
    this.notify()
  }
}
class Watcher {
  update(dep) {
    console.log(dep.state)
  }
}

const dep = new Dep()
const watcher = new Watcher()
dep.addSub(watcher)

// #4
dep.setState(47)
