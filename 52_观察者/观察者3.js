// 1. Observer 对象负责对 data 中的数据进行递归劫持，都转换成 getter/setter 的形式。

// 2. 初始化数据的时候，就会触发 getter，调用 dep.addSub 添加 watcher。

// 3. 当数据变化后会触发 settter，会调用 dep.notify 方法发送通知，然后再调用 watcher 的 update 方法去更新视图；
