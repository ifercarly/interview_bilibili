// 观察者模式：当状态变化时，通知到所有的观察者进行更新
// 【目标】
class Dep {
  constructor() {
    this.subs = []
  }
  // !添加观察者
  addSub(sub) {
    if (sub && sub.update) {
      this.subs.push(sub)
    }
  }
  // ?通知观察者
  notify() {
    this.subs.forEach((sub) => sub.update())
  }
}
// 【观察者】
class Watcher {
  update() {
    console.log('update')
  }
}

const dep = new Dep()
const watcher = new Watcher()
// !添加观察者
dep.addSub(watcher)
// ?通知观察者
dep.notify()
