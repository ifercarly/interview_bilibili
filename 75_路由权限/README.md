## 问题：路由权限怎么做的？

0️⃣ 从后端获取当前用户的<font color=e32d40>**权限标识**</font>（时机：用户登录成功后，跳转到首页之前，一般会在全局前置路由导航守卫中做），假如返回的结果如下。

时机为什么是登录后：因为只有登录后才有可能根据 uid 或 token 来区分出是哪个用户。

时机为什么是跳到首页之前：因为我们希望跳转到首页的一刹那就可以直接使用根据权限标识筛选出来的权限路由。

```bash
['employees', 'settings']
```

1️⃣ 前端根据后端返回的权限标识从准备好的一份完整的动态路由列表中筛选出<font color=e32d40>**权限路由**</font>，假如筛选的结果如下。

```ts
const powerRoutes = [
    {
        path: '/employees',
        name: 'employees',
        component: () => import('@/views/employees')
    },
    {
        path: '/settings',
        name: 'settings',
        component: () => import('@/views/settings')
    }
]
```

2️⃣ 前端通过 `router.addRoutes` 方法把权限路由添加到路由实例（可以理解为把上面的路由对象怼到了路由配置项 routes 里面了），而一旦被添加到了路由实例，当前用户就具有了访问某个路由级别的权限了。

```js
router.addRoutes(powerRoutes)
```

3️⃣ 刷新的时候碰到了 404 问题？

原因：一旦进行 `addRoutes` 的操作，曾经应该配置到 routes 最后的 404 路由却跑到了非最后位置。

解决：注释掉曾经配置的 404 路由，通过下面方式把 404 路由添加到路由实例的最后面。

```ts
router.addRoutes([...otherRoutes, { path: '*', redirect: '/404', hidden: true }])
```

4️⃣ 刷新的时候碰到了白屏问题？

通过 addRoutes 后续添加的路由是“异步”的，不会马上生效，需要重新执行一下 beforeEach 的逻辑就好了。

```js
next({
    ...to,
    replace: true
})
```

5️⃣ 侧边栏问题？

问题：侧边栏的信息是通过 `this.$router.options.routes` 这个 API 获取并循环渲染的，而这个 API 对于通过 `addRoutes` 后续添加的路由配置项是拿不到的，所以展示不出来。

解决：把上面筛选的权限路由往 Vuex 也存放一份，主要目的就是给侧边栏使用。

6️⃣ 后续登录用户受影响的问题？

后续登录的用户可能会受到上个用户筛选出的路由信息的影响？

所以退出的时候，要重置一下路由信息（包含路由实例 router 和 Vuex 中的筛选过来的 routes 等信息）。

