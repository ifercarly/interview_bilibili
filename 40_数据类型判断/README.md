## typeof

typeof：除了 function，区分不出复杂数据类型（例如 []、{}、`//`、null 得到的结果都是 'object'）。

## instanceof

insanceof：A instanceof B，本质上是判断 A 是否能够通过原型链（\_\_proto\_\_） 找到 `B.prototype`。

缺点：有时候也不能准确的定位到具体是什么类型。

例如。

```js
const x = []

x instanceof Object // true
// 因为 [].__proto__.__proto__ === Object.prototype

{} instanceof Object // true
// 因为 {}.__proto__ === Object.prototype
```

```js
const x = []
x instanceof Array // true

// 上面的结果是如果是 false，则说明 x 不是数组，不是数组那具体是是什么呢？不知道
```

## constructor

作用：能判断出实例的构造函数是谁，例如。

```js
const x = []
x.constructor === Array

// 上面的结果是如果是 false，则说明 x 不是数组，不是数组那具体是是什么呢？不知道
```

只能区分是不是的问题。

## Object.prototype.toString.call

Object.prototype.toString 方法的作用：处理 toString 内部的 this （默认是实例对象 {}）为有规律的字符串，并返回。

通过上面的话我们可以分析出下面两个结论。

Object.prototype.toString 内部的 this 是【谁】，就可以返回【谁】对应的有规律的字符串。

既然是有规律的字符串，我们可以根据这个规律做出类型判断。