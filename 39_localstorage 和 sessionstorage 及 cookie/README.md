# 说一下 localStorage、sessionStorage 和 cookie 的差异

## 生命周期不一样

localStorage：永久存储，除非手动删除。

sessionStorage：关闭当前**页面**后会自动清除。

cookie：默认关闭**浏览器**后失效，如果设置了过期时间，则到达过期时间才后失效。

## 生效范围不一样

localStorage：同域都可以共享。

sessionStorage：只有当前标签页才能访问。

cookie：同域下且 path 匹配的情况下才能访问。	

什么是 path 匹配？

例如 http://www.baidu.com:3000/ 下创建的 cookie 或者设置 cookie 的时候加了 `path=/`，同域下的任意路径都能访问，因为任意路径都属于 `/` 的子路径，又称为和 `/` 是匹配的。

例如 http://www.baidu.com:3000/a/b 下创建的 cookie 或者设置 cookie 的时候加了 `path=/a`，同域下和 `/a` 匹配的任意路径都能访问，例如 `/a`、`/a/b`、`/a/c`、`/a/b/c` 等都称为是和 `/a` 匹配，或者称为是 `/a` 的子路径。

/

/b

/c

## 存储大小不一样

[MDN](https://developer.mozilla.org/zh-CN/docs/Web/API/Window/sessionStorage)

localStorage：4.98MB（不同浏览器下会有差异，例如 Safari 2.49M）。

sessionStorage：4.98MB。

cookie：4KB。

## 操作主体不一样

localStorage / sessionStorage：只有客户端才能设置。

cookie：客户端（`document.cookie`）和服务端（`Set-Cookie`）都可以设置。

## 请求是否会携带

localStorage / sessionStorage：请求时不会自动携带。

cookie：每次请求都会随着请求头带到后端（符合同域且 path 匹配的条件）。
