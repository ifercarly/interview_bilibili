function upload(blobOrFile) {
  const xhr = new XMLHttpRequest();
  xhr.open('POST', 'http://localhost:8000/upload_video', true);
  xhr.onload = function (e) {
    console.log(e.responseText)
  };
  xhr.send(blobOrFile);
}

document.querySelector('#uploadBtn').addEventListener('change', function (e) {
  const blob = this.files[0];

  const BYTES_PER_CHUNK = 1024 * 1024; // 1MB chunk sizes.
  const SIZE = blob.size;

  let start = 0;
  let end = BYTES_PER_CHUNK;

  while (start < SIZE) {
    upload(blob.slice(start, end));
    start = end;
    end = start + BYTES_PER_CHUNK;
  }
}, false);