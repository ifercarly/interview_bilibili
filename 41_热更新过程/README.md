依赖了 hot-module-replacement 插件

1. 使用 WDS（webpack-dev-server） 托管静态资源，同时注入 HMR（hot module replacement） 代码。

2. 浏览器加载页面后，与 WDS 建立 WebSocket（双向数据通信的协议） 连接。

3. Webpack 监听到文件变化后，增量构建发生变更的模块，并通过 WebSocket 发送 hash 事件。

4. 浏览器接收到 hash 事件后，请求 manifest 资源文件，确认增量变更范围。

5. 浏览器加载发生变更的增量模块。

6. Webpack 触发变更模块的 module.hot.accept 回调，执行代码变更逻辑。

