refresh_token，accesstoken

```ts
const result = ref({})

// #1 准备一个全局变量，为了装取消请求的函数
let cancel = null

const getSuggestion = async () => {
    const res = await request.get('/suggestion', {
        params: {
            name: 'xxx',
        },
        // #2 把取消当前请求的 c 函数给了全局变量 cancel
        cancelToken: new axios.CancelToken((c) => (cancel = c)),
    })

    result.value = res.data
}

oBtn.onclick = function() {
    // #3 点击按钮的时候关闭请求
    cancel && cancel()
    getSuggestion()
}

// utils/request.js
request.interceptors.response.use(
    function (response) {
        return response
    },
    async function (error) {
        // #4 请求拦截器处给出提示
        // if (error.name === 'CanceledError') {
        if (axios.isCancel(error)) {
            Toast.show('请求过快，你是机器人吗')
            return Promise.reject(error)
        }
        return Promise.reject(error)
    }
)
```
