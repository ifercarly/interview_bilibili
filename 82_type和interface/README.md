# type 和 interface 的异同

##  1. 都能描述基本对象

都能描述对象，但 type 还能描述其他任意类型，例如 string、number 等基本类型、联合或交叉等类型。

```ts
// 都能描述对象
interface IPerson {
  name: string
  age: number
}

type TPerson = {
  name: string
  age: number
}

const person: TPerson = {
  name: 'i47',
  age: 18
}
```

```ts
// 期望描述一个性别，可能是数字或字符串
type 字符串或数字 = string | number

const sex: 字符串或数字 = 0
```

## 2. 都能进行类型拓展

都能进行类型拓展，interface 通过 extends 来实现，type 通过 & 交叉运算符号形成交叉类型。

```ts
// interface
// 项目中已经存在的，且其他地方被用到的 I2d
interface I2d {
  x: number
  y: number
}

interface I3d extends I2d {
  z: number
}

const the3: I3d = {
  x: 4,
  y: 7,
  z: 5
}
```

```ts
// type
// 项目中已经存在的，且其他地方被用到的 T2d
type T2d = {
  x: number
  y: number
}

type T3d = T2d & {
  z: number
}

const the3: T3d = {
  x: 4,
  y: 7,
  z: 5
}
```

## 3. 名字相同时的表现

相同的 interface 会合并，相同的 type 会报错。

```ts
interface IPerson {
  name: string
  age: number
}

interface IPerson {
  money: number
}


const person: IPerson = {
  name: 'elser',
  age: 18,
  money: 888
}
```

```ts
type TPerson = {
  name: string
  age: number
}

type TPerson = {
  name: string
  age: number
}

// Duplicate identifier 'TPerson'.
```



🤔 利用同名的 interface 会合并这个特性，可以自由的拓展第三方库的类型，例如在使用 Vue Router 的时候给路由元信息定义类型时。

```ts
const router = createRouter({
  	history: createWebHistory(import.meta.env.BASE_URL),
  	routes: [
    	{
      		path: '/room',
      		component: () => import('@/views/Home/index.vue'),
      		meta: {
        		title: '就业发送机'
      		}
    	}
  	]
})
```



```ts
import 'vue-router';
declare module 'vue-router' {
    interface RouteMeta {
        title?: string
    }
}
```

## 4. 一般应该怎样去用

个人一般使用 interface 来描述对象结构，用 type 来描述类型之间的关系。

```ts
interface IPerson {
  name: string
  age: number
}

type TTemp = '男' | '女' | 0 | 1
```
