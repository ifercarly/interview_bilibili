# for in / for of 的本质区别是什么？

[MDN](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Statements/for...of#for...of%E4%B8%8Efor...in%E7%9A%84%E5%8C%BA%E5%88%AB)



## 适用的目标不一样

for in 适用于**可枚举数据**，例如对象、数组、字符串。

🤔 什么是可枚举呢？

属性的 enumerable 值为 true 咱就称为是可枚举的，通过 `Object.getOwnPropertyDescriptors()` 验证/查看。



for of 适用于**可迭代数据**，例如 Array、String、Map、Set、TypedArray、函数的 arguments 对象、NodeList 对象。

🤔 什么是可迭代？

ES6 中，具有 `Symbol.iterator` 属性，它对应的值是一个函数，调用这个函数后可以得到一个对象，每次调用对象的 next 方法能得到目标的每一项，只要符合这个特点就是可迭代的。



## 遍历的范围不一样

for in 原型上的可枚举的属性也能被遍历到。

for of 一般只能遍历自身的（具体和迭代器的内部实现有关）。

```js
arr[Symbol.iterator] = function() {
  	const _this = this
  	return {
    	i: 0,
    	next() {
      		return this.i < _this.length ? { value: _this[this.i++], done: false } : { value: undefined, done: true }
    	}
  	};
}
```



## 得到结果的不一样

for in 得到的是 key（并且不保证顺序）；

for of 一般得到的是 value（具体和迭代器的内部实现有关）。

```js
const arr = [3, 5, 7];
arr[Symbol.iterator] = function() {
  	const _this = this
  	return {
    	i: 0,
    	next() {
      		return this.i < _this.length ? { value: _this[this.i++], done: false } : { value: undefined, done: true }
    	}
  	};
}
```











