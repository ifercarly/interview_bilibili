const arr = [7]
arr[Symbol.iterator] = function() {
  const _this = this
  return {
    i: 0,
    next() {
        return this.i < _this.length ? { value: this.i++, done: false } : { value: undefined, done: true }
    }
  };
}
for(let val of arr) {
  console.log(val)
}