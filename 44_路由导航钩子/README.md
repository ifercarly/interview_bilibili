# 说一下 Vue Router 有几种类型钩子？具体是什么，应用是什么？

[官方](https://router.vuejs.org/zh/guide/advanced/navigation-guards.html)

[完整的导航解析流程](https://router.vuejs.org/zh/guide/advanced/navigation-guards.html#%E5%AE%8C%E6%95%B4%E7%9A%84%E5%AF%BC%E8%88%AA%E8%A7%A3%E6%9E%90%E6%B5%81%E7%A8%8B)

[图解](https://www.processon.com/view/link/62de2fcaf346fb4e907a6085)

[代码测试](https://gitee.com/ifercarly/interview/blob/master/resource/46/README.md)