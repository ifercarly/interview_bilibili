const express = require('express')
const cors = require('cors')

const app = express()
app.use(cors())

app.get('/', (req, res) => {
  // 后端返回的是 JSON 格式的字符串，字符串中的数字超出了 2 的 53 次方，即大数字
  // exceed Math.pow(2, 53)
  res.end('{ "id": 900719925474099288 }')
})

app.listen(3001, () => console.log('Server running on http://localhost:3001'))
