## 开发中碰到过什么问题，是怎么解决的？



## 大数字问题

🤨 什么是大数字

JS 能表示的最大<font color=e32d40>**安全**</font>数值区间是 -Math.pow(2, 53) + 1 ~ Math.pow(2, 53) - 1，超过这个区间的数字称为大数字。

🤨 问题描述

从文章列表，根据文章列表项的 ID 进入到文章详情的时候，有的时候会出现 404。

🤨 原因分析

后端返回的数据大部分是 JSON 格式的字符串，为了方便我们使用，axios 内部会对这个数据进行 JSON.parse 反序列化的操作；

而当这个 JSON 格式的字符串中包含大数字的时候，JSON.parse 是搞不定的，会出现转出来的结果和原来不一致的问题；

🤨 解决方案

和后端协商，返回的 ID 使用字符串的形式进行表示；

前端配置 axios 的 transformResponse 选项，手动使用一些第三方包对后端返回的数据进行处理，例如 json-bigint。

























## 问题演示

### 有问题代码

后端代码

```js
const express = require('express')
const cors = require('cors')

const app = express()
app.use(cors())

app.get('/', (req, res) => {
  // 后端返回的是 JSON 格式的字符串，字符串中的数字超出了 2 的 53 次方，即大数字
  // exceed Math.pow(2, 53)
  res.end('{ "id": 900719925474099288 }')
})

app.listen(3001, () => console.log('Server running on http://localhost:3001'))
```

前端代码

`utils/request.ts`

```js
import axios from 'axios'

const request = axios.create({
  baseURL: 'http://localhost:3001',
})

export default request
```

`App.vue`

```html
<script setup lang="ts" name="App">
import request from './utils/request'

request.get('/').then(({ data }) => {
  console.log(data)
})
</script>

<template>
  <h1>大数字测试</h1>
</template>
```

### 解决方案

`utils/request.ts`

```js
import axios from 'axios'
import JSONBigInt from 'json-bigint'

const request = axios.create({
  baseURL: 'http://localhost:3001',
  transformResponse: [
    (data) => {
      try {
        return JSONBigInt.parse(data)
      } catch (err) {
        return data
      }
    },
  ],
})

export default request
```

`App.vue`

```html
<script setup lang="ts" name="App">
import JSONBigInt from 'json-bigint'
import request from './utils/request'

request.get('/').then(({ data }) => {
  console.log(JSONBigInt.stringify(data))
})
</script>

<template>
  <h1>大数字测试</h1>
</template>
```

