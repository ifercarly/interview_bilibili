# 说一下你对 BFC 的理解？

## BFC 是什么

BFC(Block formatting contexts)，[参考 W3C 文档](https://www.w3.org/TR/CSS2/visuren.html#block-formatting)，[参考 MDN 文档](https://developer.mozilla.org/zh-CN/docs/Web/Guide/CSS/Block_formatting_context)

`块格式化上下文`，<font color=e32d40>**是一种特性**</font>（文档中的一块渲染区域），拥有自己的一套渲染规则，它决定了其**子元素**将如何布局，以及和其他元素的相互关系和作用。

## 如何触发 BFC 特性
1、根元素（`<html>`）。

2、浮动元素（元素的 float 不是 none）。

3、绝对定位元素（元素的 position 为 absolute 或 fixed）。

4、行内块元素（元素的 display 为 inline-block）。

5、表格单元格（元素的 display 为 table-cell，HTML 表格单元格默认为该值）。

6、overflow 不等于 visible。

7、更多的触发方法参考 [MDN](https://developer.mozilla.org/zh-CN/docs/Web/Guide/CSS/Block_formatting_context) 。

## BFC 特性有什么作用

1、可以包含内部浮动元素。

2、可以排除外部浮动带来的影响。

3、阻止[外边距重叠](https://developer.mozilla.org/zh-CN/docs/Web/CSS/CSS_Box_Model/Mastering_margin_collapsing)。

4、解决 margin 塌陷。

