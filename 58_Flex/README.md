[阮一峰老师](https://www.ruanyifeng.com/blog/2015/07/flex-examples.html)

## 右下角的骰子

<img src="./images/1.png"/>

```css
.box {
  width: 200px;
  height: 200px;
  border: 1px solid red;
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
}
.box span {
  width: 50px;
  height: 50px;
  background-color: pink;
  border-radius: 50%;
}
```

## 双项目

<img src="./images/2.png"/>

```css
.box {
  width: 200px;
  height: 200px;
  border: 1px solid red;
  /* #1 */
  display: flex;
  /* #2 */
  justify-content: space-between;
}

.box span {
  width: 50px;
  height: 50px;
  background-color: pink;
  border-radius: 50%;
}
```

<img src="./images/3.png"/>

```css
.box {
  width: 200px;
  height: 200px;
  border: 1px solid red;
  /* #1 */
  display: flex;
  /* #2 */
  justify-content: space-between;
  /* #3 */
  flex-direction: column;
}

.box span {
  width: 50px;
  height: 50px;
  background-color: pink;
  border-radius: 50%;
}
```

<img src="./images/4.png"/>

```css
.box {
  width: 200px;
  height: 200px;
  border: 1px solid red;
  /* #1 */
  display: flex;
  /* #2 */
  justify-content: space-between;
  /* #3 */
  flex-direction: column;
  /* #4 */
  align-items: center;
}

.box span {
  width: 50px;
  height: 50px;
  background-color: pink;
  border-radius: 50%;
}
```

<img src="./images/5.png"/>

```css
.box {
  width: 150px;
  height: 150px;
  border: 1px solid red;
  /* #1 */
  display: flex;
}

.box span {
  width: 50px;
  height: 50px;
  background-color: pink;
  border-radius: 50%;
}

.box span:nth-child(2) {
  /* #2 */
  align-self: center;
}
```

<img src="./images/6.png"/>

```css
.box {
  width: 150px;
  height: 150px;
  border: 1px solid red;
  /* #1 */
  display: flex;
  /* #2 */
  justify-content: space-between;
}

.box span {
  width: 50px;
  height: 50px;
  background-color: pink;
  border-radius: 50%;
}

.box span:nth-child(2) {
  /* #3 */
  align-self: flex-end;
}
```

## 三项目

<img src="./images/7.png"/>

```css
.box {
  width: 150px;
  height: 150px;
  border: 1px solid red;
  /* #1 */
  display: flex;
}

.box span {
  width: 50px;
  height: 50px;
  background-color: pink;
  border-radius: 50%;
}

.box span:nth-child(2) {
  /* #2 */
  align-self: center;
}

.box span:nth-child(3) {
  /* #3 */
  align-self: flex-end;
}
```

## 四项目


<img src="./images/8.png"/>

```css
.box {
  width: 150px;
  height: 150px;
  border: 1px solid red;
  /* #1 */
  display: flex;
  /* #2 */
  justify-content: flex-end;
  /* #3 主轴多行 */
  align-content: space-between;
  /* #4 */
  flex-wrap: wrap;
}

.box span {
  width: 50px;
  height: 50px;
  background-color: pink;
  border-radius: 50%;
}
/* #3 */
/* .box span:nth-child(4) {
  align-self: flex-end;
} */
```

<img src="./images/9.png"/>

```html
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <style>
    .box {
      width: 150px;
      height: 150px;
      border: 1px solid red;
      /* #3 */
      display: flex;
      /* #4 */
      flex-wrap: wrap;
      /* #6 */
      align-content: space-between;
    }

    .column {
      /* #5 */
      flex-basis: 100%;
      /* #1 */
      display: flex;
      /* #2 */
      justify-content: space-between;
    }

    .column span {
      width: 50px;
      height: 50px;
      background-color: pink;
      border-radius: 50%;
    }
  </style>
</head>

<body>
  <div class="box">
    <div class="column">
      <span class="item"></span>
      <span class="item"></span>
    </div>
    <div class="column">
      <span class="item"></span>
      <span class="item"></span>
    </div>
  </div>
</body>

</html>
```