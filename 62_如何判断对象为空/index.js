const o = {
    a: undefined,
    b: function () {},
    c: Symbol(),
}
console.log(JSON.stringify(o) === '{}')