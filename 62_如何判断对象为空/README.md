# 如何判断对象为空？

## 1. JSON.stringify(target)

```js
const o = {}
console.log(JSON.stringify(o) === '{}')
```

[问题](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify)

```js
const o = {
  	a: undefined,
  	b: function () {},
  	c: Symbol(),
}
console.log(JSON.stringify(o) === '{}')
```



## 2. for in 或 Object.keys(target)

for in 配合 hasOwnProperty 遍历对象。

```js
const o = {}
const isEmptyObject = (o) => {
  	let bBar = true // true 表示是空
  	for (let attr in o) {
        if(o.hasOwnProperty(attr)) {
            bBar = false
            break
        }
  	}
  	return bBar
}
console.log(isEmptyObject(o)) // true
```



用 [Object.keys()](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/keys)，会返回此对象自身可枚举在属性组成的数组，效果同上。



🤔 上面两种方式的问题：不可枚举的搞不定。

```js
const o = {}
Object.defineProperty(o, 'name', {
  	value: '28',
  	enumerable: false,
})
console.log(isEmptyObject(o)) // true
console.log(Object.keys(o).length === 0) // true
```



## 3. Object.getOwnPropertyNames(target)

搞定了不可枚举的，[MDN](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/getOwnPropertyNames)

```js
const o = {}
Object.defineProperty(o, 'name', {
  	value: '28',
  	enumerable: false,
})
console.log(Object.getOwnPropertyNames(o)) // ['name']
```



## 4. Object.getOwnPropertySymbols(target)



```js
const s = Symbol()

const o = {
    [s]: 28,
    name: 48
}
console.log(Object.getOwnPropertyNames(o).concat(Object.getOwnPropertySymbols(o)).length === 0)
```



## 5. Reflect.ownKeys(target)

https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Reflect/ownKeys

到此：搞定了上面的所有问题。

```js
const s = Symbol()
const o = {
  	[s]: 233
}
Object.defineProperty(o, 'name', {
  	value: '28',
  	enumerable: false,
})

console.log(Reflect.ownKeys(o).length === 0)
```
