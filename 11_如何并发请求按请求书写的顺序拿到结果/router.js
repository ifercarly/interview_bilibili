const express = require('express')
const router = express.Router()

const sleep = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

router.get('/one', async (req, res) => {
  await sleep(1000)
  res.status(500).send('Server error')
})

router.get('/two', async (req, res) => {
  await sleep(5000)
  res.send({
    time: '1000ms',
  })
})

router.get('/three', async (req, res) => {
  await sleep(3000)
  res.send({
    time: '3000ms',
  })
})

module.exports = router
