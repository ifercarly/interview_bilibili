## 如何并发请求，按请求书写的顺序拿到对应的结果？

```js
Promise.all([p1, p2, p3])
  .then((r) => {
    console.log(r) // [r1 的结果, r2 的结果, r3 的结果]
  })
  .catch((e) => {
    console.log(e)
    // return Promise.resolve(undefined)
  })
  .then((r) => {
    console.log(r)
  })
```

## 如果中间有一个出错了会怎样？

```js
// 触发 catch
```

## 触发 catch 后，后面再写 then 会被触发吗？为什么？

正常会的，因为 catch 函数的内部默认也会返回一个新的 Promise。
