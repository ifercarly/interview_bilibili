# 兄弟通信

需求：点击 Foo 组件中的按钮，让 Bar 组件中的数据加 +1，它们两者是兄弟关系。

## 状态提升

把需要操作的数据提升到公共的父组件，然后再转换成一种父子、子父的数据传递关系。

![image-20220806202835304](README.assets/image-20220806202835304.png)















代码如下。

![image-20220806202652668](README.assets/image-20220806202652668.png)





























## Event Bus

🏀 注意：Vue3 移除了自带的 EventBus，可以像下面一样自己实现或者使用官方[推荐的库](https://v3-migration.vuejs.org/breaking-changes/events-api.html#event-bus)。

利用发布订阅的设计模式，一个组件通过 $emit 触发自定义事件并传值，另一个组件通过 $on 监听被触发自定义事件并接收数据。

```js
class EventEmitter {
  constructor() {
    this.subs = Object.create(null)
  }
  $on(type, handler) {
    this.subs[type] ? this.subs[type].push(handler) : (this.subs[type] = [handler])
  }
  $emit(type, ...args) {
    this.subs[type].forEach((handler) => handler(...args))
  }
}
```

![image-20220806215517191](README.assets/image-20220806215517191.png)



## Vuex

