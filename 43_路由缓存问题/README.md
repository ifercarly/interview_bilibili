# 你在开发中碰到过什么问题，是怎么解决的？

## 路由缓存问题

**当路由地址的切换匹配的是同一个 path 时**，Vue 出于性能的考虑，对应的路由组件会被<font color=e32d40>**复用**</font>。

也就意味着，即便路由参数每次发生了变化，路由组件中相应的生命周期钩子也就只会被触发 1 次。

例如当在 setup 中需要根据路由参数的变化发请求时，会发现拿到的永远是最初的旧数据。

## 解决方案

1\. 给路由出口的地方加 key。

```html
<router-view :key="$route.fullPath" />
```

2\. 通过 watch 监听 route 的变化，然后重新获取数据。

```ts
watch(
  () => route.params.id,
  (newId) => {
    getDetail(newId)
  }
)
```

3\. onBeforeRouteUpdate 中拿到最新的路由参数重新发请求。

```ts
onBeforeRouteUpdate((to) => {
  getDetail(to.params.id as string)
})
```
