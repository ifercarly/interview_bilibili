# 问：功能或按钮级别的权限你是怎么做的？

1\. 可以通过 `Vue.directive` 定义一个全局的自定义指定。

做按钮功能控制的使用，可以使用自定义指令传递过来一个**功能标识**，在自定义指令的 inserted 阶段通过 binding.value 拿到功能标识，内部看一下在不在后端返回的当前用户的功能数组里面，不在的话就对这个 DOM 进行禁用或删除的操作。

```js
Vue.directive('check',{
    inserted(el, binding){
        const list = ['DEPARS_DELETE', 'DEPARS_ADD']
        if(!list.includes(binding.value)){
			el.remove()
        }
    },
})
```

```html
 <button v-check="DEPARS_DELETE">删除部门</button>
```

2\. 可以通过 Vue.mixin 全局混入一个方法来进行处理。

```js
Vue.mixin({
    methods: {
        checkPermission: function(tag) {
            // 后端返回的功能列表
	    	const list = ['DEPARS_DELETE', 'DEPARS_ADD']
    		return list.includes(tag)
        }
    }
})
```

```html
 <button v-if="checkPermission('DEPARS_DELETE')">删除用户</button>
```

