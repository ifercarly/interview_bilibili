# v-for 和 v-if 为什么不建议放一行

## Vue2

因为 Vue2 中 v-for 的优先级比 v-if 高，Vue 会先循环出所有的元素，再把不符合条件的销毁，多了一些没必要的创建和销毁的操作，性能浪费。

## Vue3

Vue3 中 v-if 比 v-for 的优先级高，更不能放一行了，因为往往 v-if 判断的时候需要依赖循环项进行，那这个时候循环项还没有呢，所以会直接报错。

## 如何解决

解决方式就是通过计算属性，先计算出需要循环的那些元素就好啦。























## 代码解释

需求：数组中的 angular 不需要渲染。

```html
<template>
  <div id="app">
    <ul>
      <li v-for="(item, index) in arr" :key="index" v-if="item !== 'angular'">{{ item }}</li>
    </ul>
  </div>
</template>

<script>
  export default {
    name: 'App',
    data() {
      return {
        arr: ['vue', 'react', 'angular'],
      }
    },
  }
</script>
```

解决

```html
<template>
  <div id="app">
    <ul>
      <li v-for="(item, index) in renderArr" :key="index">{{ item }}</li>
    </ul>
  </div>
</template>

<script>
export default {
  name: 'App',
  data() {
    return {
      arr: ['vue', 'react', 'angular'],
    }
  },
  computed: {
    renderArr() {
      return this.arr.filter((item) => item !== 'angular')
    },
  },
}
</script>
```

