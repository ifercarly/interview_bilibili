## 如何监听子组件生命周期的某个阶段，父组件做一些相关操作？

### 方法 1：借助自定义事件

父组件通过自定义事件，绑定某个回调函数。

`App.vue`

```html
<script setup lang="ts">
  import HelloWorld from './HelloWorld.vue'
  const logHello = () => {
    console.log('Hello')
  }
</script>

<template>
  <HelloWorld @logHello="logHello" />
</template>
```

子组件在对应的生命周期钩子里面进行触发，`HelloWorld.vue`。

```html
<script setup lang="ts">
  import { onMounted } from 'vue'

  const emits = defineEmits<{
    (e: 'logHello'): void
  }>()

  onMounted(() => {
    emits('logHello')
  })
</script>
<template>
  <div>Hello</div>
</template>
```

### 方法 2：通过 @vnode- 直接进行监听

`App.vue`

```html
<script setup lang="ts">
  import HelloWorld from './HelloWorld.vue'
  const logHello = () => {
    console.log('Hello')
  }
</script>

<template>
  <HelloWorld @vnode-mounted="logHello" />
</template>
```

`HelloWorld.vue`

```html
<template>
  <div>Hello</div>
</template>
```
