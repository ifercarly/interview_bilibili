# hash 和 history 路由的异同？

两者都是用来实现前端路由的（前端路由就是页面地址和组件之间的对应关系）。

## 兼容性

hash 兼容到 IE8，history 兼容到 IE10。

## 实现原理

hash 模式是通过监听 onhashchange 事件做的处理。

history 模式是利用 H5 新增的 History 相关 API 实现的，例如 onpopstate 事件、pushState、replaceState 等方法。

## 刷新页面时，对于后端的表现

刷新页面时，hash 地址也就是（也就是 # 号后面的内容），不会作为资源发送到服务端，后端拿到的都是 `/` 这个请求地址，它只需要返回 `index.html` 页面就好啦。

```js
https://www.baidu.com/#/news
https://www.baidu.com/#/user
```

刷新页面时，history 地址对于服务端来说是一个新的请求，后端拿到的是不同的请求地址，也就意味着需要服务端对这些请求做处理，否则会 404。

```js
https://www.baidu.com/news
https://www.baidu.com/user
```

做什么处理呢？匹配到相关 GET 请求，统一返回 `index.html`，`index.html` 加载的有路由相关的代码，所以也就转换为由前端路由来处理啦。
