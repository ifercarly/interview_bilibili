## 一个结论

```js
Promise.resolve(Promise.resolve(4)).then((r) => console.log(r))
```

等价写面如下。

```js
new Promise((resolve) => {
  Promise.resolve().then(() => {
    Promise.resolve(4).then((r) => {
      resolve(r)
    })
  })
}).then((r) => console.log(r))
```

---

```js
new Promise((resolve) => {
  resolve(Promise.resolve(4))
}).then((r) => console.log(r))
```

等价写面如下。

```js
new Promise((resolve) => {
  new Promise(() => {
    Promise.resolve().then(() => {
      Promise.resolve(4).then((r) => {
        resolve(r)
      })
    })
  })
}).then((r) => console.log(r))
```

## 问题 1

```js
Promise.resolve()
  .then(() => {
    console.log(0)
    // 碰到 then 中直接返回 Promise 的情况进行改写
    return Promise.resolve(4)
  })
  .then((res) => {
    console.log(res)
  })

Promise.resolve()
  .then(() => {
    console.log(1)
  })
  .then(() => {
    console.log(2)
  })
  .then(() => {
    console.log(3)
  })
  .then(() => {
    console.log(5)
  })
  .then(() => {
    console.log(6)
  })
```

改写如下。

```js
Promise.resolve().then(() => {
  console.log(0)
  new Promise((resolve) => {
    Promise.resolve().then(() => {
      Promise.resolve(4).then((r) => {
        resolve(r)
      })
    })
    // 这儿可并没有所谓的 return Promise.resolve(undefined)
  }).then((res) => {
    console.log(res)
  })
})

Promise.resolve()
  .then(() => {
    console.log(1)
  })
  .then(() => {
    console.log(2)
  })
  .then(() => {
    console.log(3)
  })
  .then(() => {
    console.log(5)
  })
  .then(() => {
    console.log(6)
  })
```

## 问题 2

```js
new Promise((resolve) => {
  let resolvedPromise = Promise.resolve()
  // 这儿 resolve 会触发后面的 then
  resolve(resolvedPromise)
}).then(() => {
  console.log('resolvePromise resolved')
})

Promise.resolve()
  .then(() => {
    console.log('promise1')
  })
  .then(() => {
    console.log('promise2')
  })
  .then(() => {
    console.log('promise3')
  })
```

改写如下。

```js
new Promise((resolve) => {
  new Promise(() => {
    Promise.resolve().then(() => {
      Promise.resolve().then((r) => {
        // 保证这儿的 resolve 是最外部的 resolve
        resolve()
      })
    })
  })
}).then(() => {
  console.log('resolvePromise resolved')
})

Promise.resolve()
  .then(() => {
    console.log('promise1')
  })
  .then(() => {
    console.log('promise2')
  })
  .then(() => {
    console.log('promise3')
  })
```
