```js
function upload(blobOrFile) {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', '/server', true);
  xhr.onload = function(e) { ... };
  xhr.send(blobOrFile);
}

document.querySelector('input[type="file"]').addEventListener('change', function(e) {
  var blob = this.files[0];

  const BYTES_PER_CHUNK = 1 * 1024 * 1024; // 1MB chunk sizes.
  const SIZE = blob.size;

  var start = 0;
  while(start < SIZE) {
    upload(blob.slice(start, start + BYTES_PER_CHUNK));
    start += BYTES_PER_CHUNK;
  }
}, false);
```

## 大文件上传

核心是利用 Blob.prototype.slice 方法，对源文件进行切分。

然后并发（Promise.all）上传多个小切片（标记下切片的顺序）。

服务端接受完切片时进行合并，如何知道接收完了呢？

一种办法是前端在每个切片中都携带最大切片数量字段，当服务端接受到这个数量的切片时自动合并。

也可以额外发一个请求主动通知服务端进行切片的合并。

## 断点续传

断点续传的原理在于前端/服务端需要记住已上传的切片，这样下次上传就可以跳过之前已上传的部分。

前端使用 localStorage 记录已上传切片的 hash，下次点击上传按钮时获取本地的记录，从记录处开始上传。

https://zhuanlan.zhihu.com/p/467714393
