# 为什么 nextTick 中就能拿到 DOM 更新后的数据了？

[文档](https://staging-cn.vuejs.org/api/general.html#nexttick)

```html
<script>
  import { nextTick } from 'vue'

  export default {
    data() {
      return {
        count: 0,
      }
    },
    methods: {
      async increment() {
        this.count++

        console.log(document.getElementById('counter').textContent) // 0

        nextTick(() => {
          console.log(document.getElementById('counter').textContent) // 1
        })
      },
    },
  }
</script>

<template>
  <button id="counter" @click="increment">{{ count }}</button>
</template>
```

## 代码执行过程

出于性能的考虑，每次数据变化，DOM 并不会立即更新，精简过程如下。

1\. 修改 Vue 中的 Data 时，Vue 会将所有和这个 Data 相关的 Watcher 加入到队列 Queue。

2\. 然后，<font color=e32d40>**调用 nextTick 方法**</font>，传入用于 DOM 更新的回调函数。

```js
nextTick(flushSchedulerQueue)
```

3\. 执行 nextTick 时，碰到异步代码，会把更新 DOM 的操作添加到异步任务队列。

```js
// 伪代码
functon nextTick(callback) {
  // 其实 nextTick 中就封装了相关的异步代码
  Promise.resolve().then(callback)
}
```

4\. 等当前执行栈中的同步代码执行完毕后，才会把任务队列中用于 DOM 更新的异步代码拿到执行栈中执行，所以【直接获取 DOM 的同步操作】去拿到【异步更新 DOM 中的数据的操作】是办不到的。

5\. 接下来再次碰到自己写的 nextTick，也会把对应的回调添加到任务队列。

```js
nextTick(function () {
  console.log(document.getElementById('counter').textContent) // 1
})
```

6\. 根据先进先出的原则，包含 DOM 更新操作的异步任务会先得到执行，执行完成后就生成了新的 DOM，接下来再次执行下一个异步任务时当然就能拿到更新后的 DOM 啦。



