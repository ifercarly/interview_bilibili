# 说一下你对 Promise 的理解？

## 是什么？

Promise 是 ES6 新增的函数。

## 怎么用？

一般作为一个构造函数来使用，即需要 new 一下来创建一个 Promise 实例；它里面有 3 种状态，分别是 `pending`（进行中）、`fulfilled`（已成功）、`rejected`（已失败）；成功会触发 then、失败会触发 catch；还有一个 finally 是永远都会被触发。

---

**Promise 相关的几个静态方法？**

1\. `Promise.race()`：接收多个 Promise 实例，可以得到**最先处理完毕**的结果（可能是成功，也可能是失败）。

```js
Promise.race([p1, p2, p3])
```

2\. `Promise.all()`：接收多个 Promise 实例，都成功了会触发 then，有一个失败就会触发 catch。

```js
Promise.all([p1, p2, p3]).then(r => console.log(r))
// [p1 的结果, p2 的结果, p3 的结果]
```

3\. `Promise.any()`：接收多个 Promise 实例，可以得到**最先处理成功**的结果，都失败才会触发 catch。

```js
Promise.any([p1, p2, p3])
```

## 解决了什么问题？

它解决了回调地狱的问题。

## 有没有替代方案？

Promise 虽然解决了回调地狱，但是并不能简化代码，所有一般工作中我会**配合** async/await 来使用。

