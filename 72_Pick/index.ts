interface IPerson {
  name: string
  age: number
  address: string
}

// type IP2 = Pick<IPerson, 'name' | 'age'>

type MyPick<T, K extends keyof T> = {
  [P in K]: T[P]
}
type IP2 = MyPick<IPerson, 'name' | 'age'>

const p: IP2 = {
  name: 'xxx',
  age: 18,
}
