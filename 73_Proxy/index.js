/* const o = {
  name: 'ifer'
}
const proxy = new Proxy(o, {
  get(target, key, receiver) {
    // receiver 是代理对象
    // console.log(receiver === proxy) // true
    return target[key]
  }
})
console.log(proxy.name) */

// 关于 Object.setPrototypeOf
/* function Person() {}
Person.prototype.say = function() {
  console.log('Hello World')
}
function Child() {}
Object.setPrototypeOf(Child.prototype, Person.prototype)

const c = new Child()
c.say()
console.log(c.constructor === Child) */

/* const parent = {
  value: 'ifer'
};
const proxy = new Proxy(parent, {
  get(target, key, receiver) {
    
    console.log(receiver === proxy); // false
    // receiver 不一定是 proxy 本身，也可能是继承 proxy 的那个对象
    console.log(receiver === obj); // true
    return target[key];
  },
});
 
const obj = {};
Object.setPrototypeOf(obj, proxy);
obj.value */

/* const parent = {
  name: 'ifer',
  get value() {
    return this.name;
  },
};
 
const proxy = new Proxy(parent, {
  get(target, key, receiver) {
    // 这里相当于 return target[key]
    return Reflect.get(target, key);
    // return Reflect.get(target, key, receiver);
  },
});
 
const obj = {
  name: 'elser',
};
 
Object.setPrototypeOf(obj, proxy);
 
console.log(obj.value); */

// 字符串的不可变性
