# 说一下你对 EventLoop 的理解？

**EventLoop 又叫事件循环，是单线程语言 JS 在运行代码代码时不被阻塞的一种机制；**

JS 代码的执行分为同步代码和异步代码，当碰到同步代码时直接在执行栈中执行；

当碰到异步代码并且时机符合时（例如定时器时间到了），就会把异步代码添加到任务队列中；

<font color=e32d40>**当执行栈中的同步代码执行完毕后**</font>，就去任务队列中把异步代码拿到执行栈中执行；

这种反复轮训任务队列并把异步代码拿到执行栈中执行的操作，就是 EventLoop。

```js
console.log('react')

setTimeout(() => {
  console.log('vue')
}, 1000)

console.log('angular')
// ...
// while(true) {}
// Web Worker
```
